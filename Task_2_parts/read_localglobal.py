import sys
import re

def read(path):
    al_tab_nset = []
    bottom_electrode_nset = []
    cu_bus_nset = []
    cu_tab_nset = []
    top_electrode_nset = []
    al_tab_elset = []
    bottom_electrode_elset = []
    cu_bus_elset = []
    cu_tab_elset = []
    top_electrode_elset = []
           
    with open(path) as fid:
        for line in fid:
            line = line.rstrip()
            if line=="GLOBAL TO LOCAL NODE AND ELEMENT MAPS":
                for line in fid:
                    line = line.rstrip()
                    line = line.lstrip()
                    if "node" in line:
                        for line in fid:
                            line = line.rstrip()
                            line = line.lstrip()
                            if re.findall('^\s*[0-9]', line):
                                x = list(map(str,line.split()))
                                if x[2]=='AL' and x[3]=='TAB-1':
                                    al_tab_nset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='BOTTOM' and x[3]=='ELECTRODE-1':
                                    bottom_electrode_nset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='CU' and x[3]=='BUS-1':
                                    cu_bus_nset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='CU' and x[3]=='TAB-1':
                                    cu_tab_nset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='TOP' and x[3]=='ELECTRODE-1':
                                    top_electrode_nset.append(list(map(int,x[:len(x)-2])))
                            elif "element" in line: break
                        
                    if "element" in line:
                        for line in fid:
                            line = line.rstrip()
                            line = line.lstrip()
                            if re.findall('^\s*[0-9]', line):
                                x = list(map(str,line.split()))
                                if x[2]=='AL' and x[3]=='TAB-1':
                                    al_tab_elset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='BOTTOM' and x[3]=='ELECTRODE-1':
                                    bottom_electrode_elset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='CU' and x[3]=='BUS-1':
                                    cu_bus_elset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='CU' and x[3]=='TAB-1':
                                    cu_tab_elset.append(list(map(int,x[:len(x)-2])))
                                if x[2]=='TOP' and x[3]=='ELECTRODE-1':
                                    top_electrode_elset.append(list(map(int,x[:len(x)-2])))
                            elif line=="LOCAL TO GLOBAL NODE AND ELEMENT MAPS": 
                                return al_tab_nset, bottom_electrode_nset, cu_bus_nset, cu_tab_nset, top_electrode_nset, al_tab_elset, bottom_electrode_elset, cu_bus_elset, cu_tab_elset, top_electrode_elset       