import re
import sys
import os
import linecache

def update_CLOAD(path,ts):

    cload = linecache.getline("current_load.dat",ts)
    cload = cload.split()

    with open(path) as fidm, open("Temp.inp", "w+") as fidw:
        for line in fidm:
            line = line.rstrip()
            
            if line.startswith("*Step, name=Electrical_thermal"):
                fidw.write(line)
                fidw.write("\n")
                for line in fidm:
                    line = line.rstrip()
                    if line=="** LOADS":
                        fidw.write(line)
                        fidw.write("\n")
                        break
                    if line.startswith("*"):
                        fidw.write(line)
                        fidw.write("\n")
                        continue
                    st = f"{cload[1]}, {cload[1]}, 0.04, {cload[1]},"
                    fidw.write(st)
                    fidw.write("\n")
            
            elif line.startswith("Current_load"):
                st = ''.join(["Current_load,CS,",cload[0]])
                fidw.write(st)
                fidw.write("\n")

            else:
                fidw.write(line)
                fidw.write("\n")
    os.remove(path)
    os.rename("Temp.inp", path) 