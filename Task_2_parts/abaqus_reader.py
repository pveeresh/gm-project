#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
import sys
import re

def readwrite_contactpressure(path):
    slave_bus_elec, slave_elec_tab, slave_tab_bus, slave_tab_tab = ([] for i in range(4))
    with open(path) as fidr, open("CPRESS.dat","w") as fidw:
        for line in fidr:            
            line = line.lstrip()
            if line.startswith("CONTACT OUTPUT"):
                if "BUS-ELEC" in line:
                    slave_bus_elec, line, fidr, fidw = read_slave_surf(line, fidr, fidw)

                if "ELEC-TAB" in line:
                    slave_elec_tab, line, fidr, fidw = read_slave_surf(line, fidr, fidw)   

                if "TAB-BUS" in line:
                    slave_tab_bus, line, fidr, fidw = read_slave_surf(line, fidr, fidw)   

                if "TAB-TAB" in line:
                    slave_tab_tab, line, fidr, fidw = read_slave_surf(line, fidr, fidw)


    return [i[0] for i in slave_bus_elec], [i[0] for i in slave_elec_tab], [i[0] for i in slave_tab_bus], [i[0] for i in slave_tab_tab]



def read_slave_surf(line, fidr, fidw):
    slave = []        
    for line in fidr:
        line = line.rstrip()
        line = line.lstrip() 
        if re.findall('^\s*[0-9]', line):
            x = list(map(str,line.split()))
            x = x[::len(x)-1]
            x = list(map(float,x))
            slave.append(x)           
            x = [format(i, 'f') for i in x]
            x[0] = int(float(x[0]))
            st = '   '.join(map(str,x))
            st = st.replace("-","")
            fidw.write(st)
            fidw.write("\n")
        elif line.startswith("CONTACT OUTPUT"): break
        elif line=='N O D E   O U T P U T': break
    return slave, line, fidr, fidw


def update_geometry(path_read,path_modify):
    node_r = []
    elset = []
    
    bus_elec, elec_tab, tab_bus, tab_tab = readwrite_contactpressure(path_read)

    with open(path_read) as fidr:
        for line_r in fidr:  
            line_r = line_r.lstrip()
            if line_r.startswith("N O D E   O U T P U T"):
                print('Reading nodal displacement')
                for line_r in fidr:
                    line_r = line_r.rstrip()
                    line_r = line_r.lstrip()
                    if re.findall('^\s*[0-9]', line_r):
                        x = list(map(str,line_r.split())) 
                        x = list(map(float,x))
                        x[0] = int(float(x[0]))
                        node_r.append(x)
                    elif line_r=='THE ANALYSIS HAS BEEN COMPLETED': break

    i = 0              
    with open(path_modify) as fidm, open("Current_input.inp", "w+") as fidmout:
        for line_m in fidm:
            line_m = line_m.rstrip()
            if line_m=="*Node":
                fidmout.write(line_m)
                fidmout.write("\n")
                for line_m in fidm:
                    line_m = line_m.rstrip()
                    line_m = line_m.lstrip()
                    
                    if line_m.startswith("*Element,"): break
                    
                    y = list(map(str,line_m.split(",")))
                    node_m = list(map(float,y)) 
                    node_m[0] = int(node_m[0])

                    if node_r[i][0]==node_m[0]:                 
                        node_m[1] += node_r[i][1]
                        node_m[2] += node_r[i][2]                      
                        node_m[1] = '%1.9f' % node_m[1]
                        node_m[2] = '%1.9f' % node_m[2]
                        st = ', '.join(map(str,node_m))
                        fidmout.write(st)
                        fidmout.write("\n")
                        i += 1

            if line_m.startswith("*Element,"):
                fidmout.write(line_m)
                fidmout.write("\n")
                for line_m in fidm:
                    line_m = line_m.rstrip()
                    line_m = line_m.lstrip()

                    if line_m.startswith("*Nset"): break

                    if line_m.startswith("*Element,"): 
                        fidmout.write(line_m)
                        fidmout.write("\n")
                        continue

                    z = list(map(str,line_m.split(",")))                       
                    elset.append(list(map(float,z)))
                    fidmout.write(line_m)
                    fidmout.write("\n")

            if line_m=="*System":
                fidmout.write(line_m)
                fidmout.write("\n")

                for line_m in fidm:

                    if "tab-tab_slave" in line_m:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(elset, tab_tab)
                        if elset_S1:
                            fidmout.write("*Elset, elset=_tab-tab_slave_S1 \n")
                            st = ', '.join(map(str,elset_S1))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-tab_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidmout.write("*Elset, elset=_tab-tab_slave_S2 \n")
                            st = ', '.join(map(str,elset_S2))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-tab_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidmout.write("*Elset, elset=_tab-tab_slave_S3 \n")
                            st = ', '.join(map(str,elset_S3))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-tab_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidmout.write("*Elset, elset=_tab-tab_slave_S4 \n")
                            st = ', '.join(map(str,elset_S4))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-tab_slave_S4, S4" + "\n"                
                        fidmout.write("*Surface, type=ELEMENT, name=tab-tab_slave \n")
                        fidmout.write(str_slave)

                        for line_m in fidm:
                            if re.findall('^\s*[0-9]', line_m) or "tab-tab_slave" in line_m:
                                continue
                            else:   break

                    if "bus-elec_slave" in line_m:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(elset, bus_elec)
                        if elset_S1:
                            fidmout.write("*Elset, elset=_bus-elec_slave_S1 \n")
                            st = ', '.join(map(str,elset_S1))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_bus-elec_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidmout.write("*Elset, elset=_bus-elec_slave_S2 \n")
                            st = ', '.join(map(str,elset_S2))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_bus-elec_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidmout.write("*Elset, elset=_bus-elec_slave_S3 \n")
                            st = ', '.join(map(str,elset_S3))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_bus-elec_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidmout.write("*Elset, elset=_bus-elec_slave_S4 \n")
                            st = ', '.join(map(str,elset_S4))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_bus-elec_slave_S4, S4" + "\n"                
                        fidmout.write("*Surface, type=ELEMENT, name=bus-elec_slave \n")
                        fidmout.write(str_slave)

                        for line_m in fidm:
                            if re.findall('^\s*[0-9]', line_m) or "bus-elec_slave" in line_m:
                                continue
                            else:   break

                    if "elec-tab_slave" in line_m:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(elset, elec_tab)
                        if elset_S1:
                            fidmout.write("*Elset, elset=_elec-tab_slave_S1 \n")
                            st = ', '.join(map(str,elset_S1))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_elec-tab_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidmout.write("*Elset, elset=_elec-tab_slave_S2 \n")
                            st = ', '.join(map(str,elset_S2))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_elec-tab_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidmout.write("*Elset, elset=_elec-tab_slave_S3 \n")
                            st = ', '.join(map(str,elset_S3))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_elec-tab_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidmout.write("*Elset, elset=_elec-tab_slave_S4 \n")
                            st = ', '.join(map(str,elset_S4))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_elec-tab_slave_S4, S4" + "\n"                
                        fidmout.write("*Surface, type=ELEMENT, name=elec-tab_slave \n")
                        fidmout.write(str_slave)

                        for line_m in fidm:
                            if re.findall('^\s*[0-9]', line_m) or "elec-tab_slave" in line_m:
                                continue
                            else:   break

                    if "tab-bus_slave" in line_m:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(elset, tab_bus)
                        if elset_S1:
                            fidmout.write("*Elset, elset=_tab-bus_slave_S1 \n")
                            st = ', '.join(map(str,elset_S1))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-bus_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidmout.write("*Elset, elset=_tab-bus_slave_S2 \n")
                            st = ', '.join(map(str,elset_S2))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-bus_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidmout.write("*Elset, elset=_tab-bus_slave_S3 \n")
                            st = ', '.join(map(str,elset_S3))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-bus_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidmout.write("*Elset, elset=_tab-bus_slave_S4 \n")
                            st = ', '.join(map(str,elset_S4))
                            fidmout.write(st)
                            fidmout.write("\n")
                            str_slave += "_tab-bus_slave_S4, S4" + "\n"                
                        fidmout.write("*Surface, type=ELEMENT, name=tab-bus_slave \n")
                        fidmout.write(str_slave)

                        for line_m in fidm:
                            if re.findall('^\s*[0-9]', line_m) or "tab-bus_slave" in line_m:
                                continue
                            else:   break
                    
                    fidmout.write(line_m)

                    if line_m.startswith("** MATERIALS"): break

            else:
                fidmout.write(line_m)
                fidmout.write("\n")               

def define_slave_elset(elset, slave_nset):
    elset_slave = []
    e = [k for k in elset if any(l in slave_nset for l in k)]
    for o in e:
        surf = 1
        o.append(o[1])
        for f, s in zip(o[1:], o[2:]):
            if (f in slave_nset) & (s in slave_nset):
                elset_slave.append([o[0], surf])
                break
            surf += 1

    elset_S1, elset_S2, elset_S3, elset_S4 = ([] for i in range(4))
    for ele in elset_slave:
        if ele[1]==1:
            elset_S1.append(int(ele[0]))
        elif ele[1]==2:
            elset_S2.append(int(ele[0]))
        elif ele[1]==3:
            elset_S3.append(int(ele[0]))
        elif ele[1]==4:
            elset_S4.append(int(ele[0]))

    return elset_S1, elset_S2, elset_S3, elset_S4                 


if __name__=="__main__":
    # readwrite_contactpressure("Initial_contact.dat")
    update_geometry("Mechanical.dat","Current_input_1.inp")