#
import matplotlib.pyplot as plt

def write_current_input():
    c = 1
    I = []
    t = []
    with open("Sample_1.dta") as fid, open("current_load.dat", "w+") as fidw:
        for line in fid:
            if (c<15000):
                ts = 350/(49000/140)
                if c%140==0:
                    x = float(line)
                    I.append(x)
                    x = (x/87.3)*1000
                    if not t:
                        t.append(ts)
                    else:
                        t.append(t[-1]+ts)
                    x = format(x, '0.2f')
                    x = x + " " + str(ts)
                    fidw.write(x)
                    fidw.write("\n")
            ts = 350/(49000/140)
            if (15000<c<40000):
                if c%140==0:
                    x = float(line)
                    I.append(x)
                    x = (x/87.3)*1000                   
                    t.append(t[-1]+ts)
                    x = format(x, '0.2f')
                    x = x + " " + str(ts)
                    fidw.write(x)
                    fidw.write("\n")
            ts = 350/(49000/140)
            if (c>40000):
                if c%140==0:
                    x = float(line)
                    I.append(x)
                    x = (x/87.3)*1000
                    t.append(t[-1]+ts)
                    x = format(x, '0.2f')
                    x = x + " " + str(ts)
                    fidw.write(x)
                    fidw.write("\n")

            if c==49000: break

            c += 1

    plt.plot(t,I)
    plt.xlabel("Time (ms)")
    plt.ylabel("Current (kA)")
    plt.show()       