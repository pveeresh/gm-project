import re
import sys
import os
import linecache

def update_materialstate(path,ts):
 
    with open(path) as fidm, open("Temp.inp", "w+") as fidw:
        for line in fidm:
            line = line.rstrip()
            if line=='*Instance, library=Initial_contact, instance="AL TAB-1"': 
                st = f'*Instance, library=Mechanical_1, instance="AL TAB-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line=='*Instance, library=Initial_contact, instance="BOTTOM ELECTRODE-1"': 
                st = f'*Instance, library=Mechanical_1, instance="BOTTOM ELECTRODE-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line=='*Instance, library=Initial_contact, instance="CU TAB-1"': 
                st = f'*Instance, library=Mechanical_1, instance="CU TAB-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line=='*Instance, library=Initial_contact, instance="CU BUS-1"': 
                st = f'*Instance, library=Mechanical_1, instance="CU BUS-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line=='*Instance, library=Initial_contact, instance="TOP ELECTRODE-1"': 
                st = f'*Instance, library=Mechanical_1, instance="TOP ELECTRODE-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line==f'*Instance, library=Mechanical_{ts-2}, instance="AL TAB-1"': 
                st = f'*Instance, library=Mechanical_{ts-1}, instance="AL TAB-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line==f'*Instance, library=Mechanical_{ts-2}, instance="BOTTOM ELECTRODE-1"': 
                st = f'*Instance, library=Mechanical_{ts-1}, instance="BOTTOM ELECTRODE-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line==f'*Instance, library=Mechanical_{ts-2}, instance="CU TAB-1"': 
                st = f'*Instance, library=Mechanical_{ts-1}, instance="CU TAB-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line==f'*Instance, library=Mechanical_{ts-2}, instance="CU BUS-1"': 
                st = f'*Instance, library=Mechanical_{ts-1}, instance="CU BUS-1"'
                fidw.write(st)
                fidw.write("\n")
            elif line==f'*Instance, library=Mechanical_{ts-2}, instance="TOP ELECTRODE-"': 
                st = f'*Instance, library=Mechanical_{ts-1}, instance="TOP ELECTRODE-1"'
                fidw.write(st)
                fidw.write("\n")
                                             
            else:
                fidw.write(line)
                fidw.write("\n")
    os.remove(path)
    os.rename("Temp.inp", path)