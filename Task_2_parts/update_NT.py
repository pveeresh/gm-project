import re
import sys
import os
import linecache

def update_initial_temp(path,ts):
 
    with open(path) as fidm, open("Temp.inp", "w+") as fidw:
        for line in fidm:
            line = line.rstrip()
            if line.startswith("** Name: Initial temp"):
                fidw.write(line)
                fidw.write("\n")
                for line in fidm:
                    if line.startswith("*Initial Conditions"): 
                        st = f"*Initial Conditions, type=TEMPERATURE, file=C:/Temp/GM_2D/Electrical_thermal_{ts}.odb, step=1"
                        fidw.write(st)
                        fidw.write("\n")
                    if line.startswith("**"): 
                        fidw.write(line)
                        break
                                             
            else:
                fidw.write(line)
                fidw.write("\n")
    os.remove(path)
    os.rename("Temp.inp", path)