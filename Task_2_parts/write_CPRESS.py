import sys
import re

def write_CPRESS(path):
    with open(path) as fidr, open("CPRESS.dat","w") as fidw:
        for line in fidr:            
            line = line.lstrip()
            if line.startswith("C O N T A C T   O U T P U T"):
                for line in fidr:
                    line = line.rstrip()
                    line = line.lstrip() 
                    if re.findall('^\s*[0-9]', line):
                        x = list(map(str,line.split()))
                        x = x[::len(x)-1]
                        x = list(map(float,x))          
                        x = [format(i, 'f') for i in x]
                        x[0] = int(float(x[0]))
                        st = '    '.join(map(str,x))
                        st = st.replace("-","")
                        fidw.write(st)
                        fidw.write("\n")
                    
                    elif line=='N O D E   O U T P U T': break