import sys
import re
import os
from read_localglobal import *

def global_to_local(node,path):
    al_tab_nset, bottom_electrode_nset, cu_bus_nset, cu_tab_nset, top_electrode_nset = read(path)[0:5]

    al_tab, bot_elec, cu_bus, cu_tab, top_elec = ([] for i in range(5))

    for i in node:
        for n in al_tab_nset:
            if i[0]==n[0]:
                i[0]=n[1]
                al_tab.append(i)
        for n in bottom_electrode_nset:
            if i[0]==n[0]:
                i[0]=n[1]
                bot_elec.append(i)
        for n in cu_bus_nset:
            if i[0]==n[0]:
                i[0]=n[1]
                cu_bus.append(i)
        for n in cu_tab_nset:
            if i[0]==n[0]:
                i[0]=n[1]
                cu_tab.append(i)
        for n in top_electrode_nset:
            if i[0]==n[0]:
                i[0]=n[1]
                top_elec.append(i)

    return al_tab, bot_elec, cu_bus, cu_tab, top_elec
            


def read_disp(path_read):
    node_r = []
    with open(path_read) as fidr:
        for line_r in fidr:  
            line_r = line_r.lstrip()
            if line_r.startswith("N O D E   O U T P U T"):
                for line_r in fidr:
                    line_r = line_r.rstrip()
                    line_r = line_r.lstrip()
                    if re.findall('^\s*[0-9]', line_r):
                        x = list(map(str,line_r.split())) 
                        x = list(map(float,x))
                        x[0] = int(float(x[0]))
                        node_r.append(x)
                        
                    elif line_r=="THE ANALYSIS HAS BEEN COMPLETED": break
    return node_r

def update_elset(path_read,path_modify):
    node_dat = read_disp(path_read)
    al_tab_u, bot_elec_u, cu_bus_u, cu_tab_u, top_elec_u = global_to_local(node_dat,path_read)
    elset = []
               
    with open(path_modify) as fidm, open("Temp.inp", "w+") as fidw:
        for line_m in fidm:
            line_m = line_m.rstrip()

            if line_m=="*Node":
                fidw.write(line_m)
                fidw.write("\n")

                if "Al tab-1" in prev_line:
                    fidm,fidw = update_nset(line_m,al_tab_u,fidm,fidw)
                if "Bottom electrode-1" in prev_line:
                    fidm,fidw = update_nset(line_m,bot_elec_u,fidm,fidw)
                if "Cu bus-1" in prev_line:
                    fidm,fidw = update_nset(line_m,cu_bus_u,fidm,fidw)
                if "Cu tab-1" in prev_line:
                    fidm,fidw = update_nset(line_m,cu_tab_u,fidm,fidw)
                if "Top electrode-1" in prev_line:
                    fidm,fidw = update_nset(line_m,top_elec_u,fidm,fidw)                                              
 
            else:
                fidw.write(line_m)
                fidw.write("\n")
    
            prev_line = line_m
    os.remove(path_modify)
    os.rename("Temp.inp", path_modify)

def update_nset(line_m,node_r,fidm,fidw):
    i = 0
    for line_m in fidm:
        line_m = line_m.rstrip()
        line_m = line_m.lstrip()
        
        if line_m.startswith("*Element,"): 
            fidw.write(line_m)
            fidw.write("\n")            
            break
        
        y = list(map(str,line_m.split(",")))
        node_m = list(map(float,y)) 
        node_m[0] = int(node_m[0])

        if node_r[i][0]==node_m[0]:                 
            node_m[1] += node_r[i][1]
            node_m[2] += node_r[i][2]                      
            node_m[1] = '%1.9f' % node_m[1]
            node_m[2] = '%1.9f' % node_m[2]
            st = ', '.join(map(str,node_m))
            fidw.write(st)
            fidw.write("\n")
            i += 1
    return fidm, fidw 