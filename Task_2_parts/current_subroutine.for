      program main
       call gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1  msname,coords,noel,node,npred,kstep,kinc)

       call gapelectr(sigma,d,temp,predef,time,ciname,slname,
     1  msname,coords,node,npred,kstep,kinc)
      end program main

      subroutine read(nn,cpress)
       dimension nn(300),cpress(300)

       open(unit=66, file="c:\\temp\\Task_2\\cpress.dat", iostat=ios)
       if (ios /= 0) stop "error"

       n = 0
       do
        read(66,'(a)', iostat=ios) line
        if (ios /= 0) exit
        n = n + 1
       end do

       rewind(66)

       do i = 1, n
        read(66,'(i5,f15.10)') nn(i),cpress(i)
       end do  
       close(66)

       return
      end subroutine read

      subroutine gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1 msname,coords,noel,node,npred,kstep,kinc)

       include 'aba_param_dp.inc'

       character*80 ciname,slname,msname
       real cpress
       integer nn

       dimension ak(5),d(2),flowm(2),temp(2),predef(2,*),nn(300),
     1  cpress(300),coords(2,2)  

       call read(nn,cpress)

       do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
       end do

       if (temp(1) == 0.0) then
        temp(1) = 25.0
       end if
       if (temp(2) == 0.0) then
        temp(2) = 25.0
       end if
       
       p = d(2)
       t = (temp(1)+temp(2))/2.0
      
       if (ciname == 'CU-CU') then
         if ((t .gt. 0.0) .and. (t .le. 20.0)) then
          ecr = 3*(210.74/p)*(0.0000264+0.0000168)*0.5
         else if ((t .gt. 20.0) .and. (t .le. 200.0)) then
          ecr = 3*(181.49/p)*(0.0000399+0.0000309)*0.5
         else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
          ecr = 3*(126.48/p)*(0.0000505+0.0000379)*0.5
         else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
          ecr = 3*(65.93/p)*(0.0000619+0.0000452)*0.5
         else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
          ecr = 3*(26.31.0/p)*(0.0000699+0.0000527)*0.5
         else if (t .gt. 500.0) then
          ecr = 3*(1.0/p)*(0.00008+0.0000605)*0.5
         else
          write(*,*) 'none'
         end if

       else if (ciname == 'CU-AL') then
         ecr_20 = 1.93*exp(-8.0)*p**(-2.0/3.0) + 
     1    8.1*exp(-9.0)*p**(-0.5) + 1.25*exp(-9.0)*p**(-1.0/3.0)

         if ((t .gt. 0.0) .and. (t .le. 20.0)) then
          ecr = ecr_20
         else if ((t .gt. 20.0) .and. (t .le. 200.0)) then
          ecr = ecr_20*(142.5/178.1)*(0.0000399+0.0000625)
     1     /(0.0000264+0.000041)
         else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
          ecr = ecr_20*(81.1/178.1)*((0.0000505+0.0000764)
     1     /(0.0000264+0.000041))
         else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
          ecr = ecr_20*(25.3/178.1)*((0.0000619+0.0000929)
     1     /(0.0000264+0.000041))
         else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
          ecr = ecr_20*(5.7/178.1)*((0.0000699+0.0001108)
     1     /(0.0000264+0.000041))
         else if (t .gt. 500.0) then
          ecr = ecr_20*(0.57/178.1)*((0.00008+0.0002808)
     1     /(0.0000264+0.000041))
         else
          write(*,*) 'none'
         end if        
       end if

       tcr = ecr/(0.0000000244*(t+273))
       ak(1) = 1.0/tcr

      return
      end subroutine gapcon

      subroutine gapelectr(sigma,d,temp,predef,time,ciname,
     1 slname,msname,coords,node,npred,kstep,kinc)

      include 'aba_param_dp.inc'

      character*80 ciname,slname,msname
      real cpress
      integer nn

      dimension sigma(5),d(2),temp(2),predef(2,*),time(2),
     1 coords(2,2),nn(300),cpress(300)

      call read(nn,cpress)

      do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
      end do

      if (temp(1) == 0.0) then
       temp(1) = 25.0
      end if
      if (temp(2) == 0.0) then
       temp(2) = 25.0
      end if

      p = d(2)
      t = (temp(1)+temp(2))/2.0
      
      if (ciname == 'CU-CU') then
       if ((t .gt. 0.0) .and. (t .le. 20.0)) then
        ecr = 3*(210.74/p)*(0.0000264+0.0000168)*0.5
       else if ((t .gt. 20.0) .and. (t .le. 200.0)) then
        ecr = 3*(181.49/p)*(0.0000399+0.0000309)*0.5
       else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
        ecr = 3*(126.48/p)*(0.0000505+0.0000379)*0.5
       else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
        ecr = 3*(65.93/p)*(0.0000619+0.0000452)*0.5
       else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
        ecr = 3*(26.31.0/p)*(0.0000699+0.0000527)*0.5
       else if (t .gt. 500.0) then
        ecr = 3*(1.0/p)*(0.00008+0.0000605)*0.5
       else
        write(*,*) 'none'
       end if

      else if (ciname == 'CU-AL') then
       ecr_20 = 1.93*exp(-8.0)*p**(-2.0/3.0)+8.1*exp(-9.0)*p**(-0.5)+ 
     1  1.25*exp(-9.0)*p**(-1.0/3.0)

       if ((t .gt. 0.0) .and. (t .le. 20.0)) then
        ecr = ecr_20
       else if ((t .gt. 20.0) .and. (t .le. 200.0)) then
        ecr = ecr_20*(142.5/178.1)*((0.0000399+0.0000625)
     1   /(0.0000264+0.000041))
       else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
        ecr = ecr_20*(81.1/178.1)*((0.0000505+0.0000764)
     1   /(0.0000264+0.000041))
       else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
        ecr = ecr_20*(25.3/178.1)*((0.0000619+0.0000929)
     1   /(0.0000264+0.000041))
       else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
        ecr = ecr_20*(5.7/178.1)*((0.0000699+0.0001108)
     1   /(0.0000264+0.000041))
       else if (t .gt. 500.0) then
        ecr = ecr_20*(0.57/178.1)*((0.00008+0.0002808)
     1   /(0.0000264+0.000041))
       else
        write(*,*) 'none'
       end if        
      end if

      sigma(1) = 1.0/ecr

      return
      end subroutine gapelectr