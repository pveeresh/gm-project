#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
import os
import shutil
from write_CPRESS import *
from get_slave_nodes import *
from update_geometry import *
from read_elset import *
from update_contact import *
from update_CLOAD import *
from update_NT import *
from current_load import *
from update_state import *

if __name__=="__main__":
    write_current_input()
    
    os.system("abaqus job=Initial_contact interactive")
    write_CPRESS("Initial_contact.dat")
    update_slave_surfaces("Initial_contact.dat", "Electrical_thermal_1.inp")
    
    for ts in range(1,350):         
        if ts>1:
            shutil.copy(f"Electrical_thermal_{ts-1}.inp",f"Electrical_thermal_{ts}.inp")           
            update_elset(f"Mechanical_{ts-1}.dat", f"Electrical_thermal_{ts}.inp")
            update_slave_surfaces(f"Mechanical_{ts-1}.dat", f"Electrical_thermal_{ts}.inp")
            update_CLOAD(f"Electrical_thermal_{ts}.inp",ts)
            update_initial_temp(f"Electrical_thermal_{ts}.inp",ts-1)
        os.system(f"abaqus job=Electrical_thermal_{ts} user=current_subroutine.for interactive")

        if ts>1:
            shutil.copy(f"Mechanical_{ts-1}.inp",f"Mechanical_{ts}.inp")
            update_materialstate(f"Mechanical_{ts}.inp",ts)
            update_initial_temp(f"Mechanical_{ts}.inp",ts)
        os.system(f"abaqus job=Mechanical_{ts}.inp interactive")
        write_CPRESS(f"Mechanical_{ts}.dat")
