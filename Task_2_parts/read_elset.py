import sys
import re

def read_elset(path_read):
	al_tab, bot_elec, cu_bus, cu_tab, top_elec = ([] for i in range(5))
		   
	with open(path_read) as fid:
		for line in fid:
			line = line.rstrip()
			if line=="*Node":
				if "Al tab-1" in prev_line:
					for line in fid:
						if line.startswith("*Element,"):
							fid, al_tab = instance(fid)
							break
				if "Bottom electrode-1" in prev_line:
					for line in fid:
						if line.startswith("*Element,"):
							fid, bot_elec = instance(fid)
							break
				if "Cu bus-1" in prev_line:
					for line in fid:
						if line.startswith("*Element,"):
							fid, cu_bus = instance(fid)
							break
				if "Cu tab-1" in prev_line:
					for line in fid:
						if line.startswith("*Element,"):
							fid, cu_tab = instance(fid)
							break
				if "Top electrode-1" in prev_line:
					for line in fid:
						if line.startswith("*Element,"):
							fid, top_elec = instance(fid)
							break

			prev_line = line
	
	return al_tab, bot_elec, cu_bus, cu_tab, top_elec
	
	

def instance(fid):
	elset = []
	for line in fid:
		line = line.rstrip()
		line = line.lstrip()

		if line.startswith("*Nset"): break

		if line.startswith("*Element,"): continue

		z = list(map(str,line.split(",")))                       
		elset.append(list(map(float,z)))

		if line.startswith("** ASSEMBLY"): break

	return fid, elset

