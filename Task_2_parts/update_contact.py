import sys
import re
import os
from get_slave_nodes import *
from read_elset import *
from read_localglobal import *

def update_slave_surfaces(path_read,path_modify):
    bus_elec, elec_tab, tab_bus, tab_tab = get_slave_nodes(path_read)
    al_tab, bot_elec, cu_bus, cu_tab, top_elec = read_elset(path_modify)
    al_tab_nset, bot_elec_nset, cu_bus_nset, cu_tab_nset, top_elec_nset = read(path_read)[0:5]

    i = 0              
    with open(path_modify) as fidm, open("Temp.inp", "w+") as fidw:
        for line in fidm:
            line = line.rstrip()
            
            if line=="** ASSEMBLY":
                fidw.write(line)
                fidw.write("\n")

                for line in fidm:

                    if "tab-tab_slave" in line:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(al_tab, tab_tab, al_tab_nset)
                        if elset_S1:
                            fidw.write('*Elset, elset=_tab-tab_slave_S1, internal, instance="AL TAB-1" \n')
                            if len(elset_S1)>15:
                                elset_split = [elset_S1[x:x+15] for x in range(0,len(elset_S1),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S1))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-tab_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidw.write('*Elset, elset=_tab-tab_slave_S2, internal, instance="AL TAB-1" \n')
                            if len(elset_S2)>15:
                                elset_split = [elset_S2[x:x+15] for x in range(0,len(elset_S2),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S2))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-tab_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidw.write('*Elset, elset=_tab-tab_slave_S3, internal, instance="AL TAB-1" \n')
                            if len(elset_S3)>15:
                                elset_split = [elset_S3[x:x+15] for x in range(0,len(elset_S3),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S3))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-tab_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidw.write('*Elset, elset=_tab-tab_slave_S4, internal, instance="AL TAB-1" \n')
                            if len(elset_S4)>15:
                                elset_split = [elset_S4[x:x+15] for x in range(0,len(elset_S4),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S4))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-tab_slave_S4, S4" + "\n"                
                        fidw.write("*Surface, type=ELEMENT, name=tab-tab_slave \n")
                        fidw.write(str_slave)

                        for line in fidm:
                            if re.findall('^\s*[0-9]', line) or "tab-tab_slave" in line:
                                continue
                            else:   break

                    if "bus-elec_slave" in line:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(bot_elec, bus_elec, bot_elec_nset)
                        if elset_S1:
                            fidw.write('*Elset, elset=_bus-elec_slave_S1, internal, instance="BOTTOM ELECTRODE-1" \n')
                            if len(elset_S1)>15:
                                elset_split = [elset_S1[x:x+15] for x in range(0,len(elset_S1),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S1))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_bus-elec_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidw.write('*Elset, elset=_bus-elec_slave_S2, internal, instance="BOTTOM ELECTRODE-1" \n')
                            if len(elset_S2)>15:
                                elset_split = [elset_S2[x:x+15] for x in range(0,len(elset_S2),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S2))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_bus-elec_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidw.write('*Elset, elset=_bus-elec_slave_S3, internal, instance="BOTTOM ELECTRODE-1" \n')
                            if len(elset_S3)>15:
                                elset_split = [elset_S3[x:x+15] for x in range(0,len(elset_S3),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S3))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_bus-elec_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidw.write('*Elset, elset=_bus-elec_slave_S4, internal, instance="BOTTOM ELECTRODE-1" \n')
                            if len(elset_S4)>15:
                                elset_split = [elset_S4[x:x+15] for x in range(0,len(elset_S4),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S4))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_bus-elec_slave_S4, S4" + "\n"                
                        fidw.write("*Surface, type=ELEMENT, name=bus-elec_slave \n")
                        fidw.write(str_slave)

                        for line in fidm:
                            if re.findall('^\s*[0-9]', line) or "bus-elec_slave" in line:
                                continue
                            else:   break

                    if "elec-tab_slave" in line:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(top_elec, elec_tab, top_elec_nset)
                        if elset_S1:
                            fidw.write('*Elset, elset=_elec-tab_slave_S1, internal, instance="TOP ELECTRODE-1" \n')
                            if len(elset_S1)>15:
                                elset_split = [elset_S1[x:x+15] for x in range(0,len(elset_S1),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S1))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_elec-tab_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidw.write('*Elset, elset=_elec-tab_slave_S2, internal, instance="TOP ELECTRODE-1" \n')
                            if len(elset_S2)>15:
                                elset_split = [elset_S2[x:x+15] for x in range(0,len(elset_S2),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S2))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_elec-tab_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidw.write('*Elset, elset=_elec-tab_slave_S3, internal, instance="TOP ELECTRODE-1" \n')
                            if len(elset_S3)>15:
                                elset_split = [elset_S3[x:x+15] for x in range(0,len(elset_S3),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S3))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_elec-tab_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidw.write('*Elset, elset=_elec-tab_slave_S4, internal, instance="TOP ELECTRODE-1"\n')
                            if len(elset_S4)>15:
                                elset_split = [elset_S4[x:x+15] for x in range(0,len(elset_S4),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S4))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_elec-tab_slave_S4, S4" + "\n"                
                        fidw.write("*Surface, type=ELEMENT, name=elec-tab_slave \n")
                        fidw.write(str_slave)

                        for line in fidm:
                            if re.findall('^\s*[0-9]', line) or "elec-tab_slave" in line:
                                continue
                            else:   break

                    if "tab-bus_slave" in line:
                        str_slave = ""
                        elset_S1, elset_S2, elset_S3, elset_S4 = define_slave_elset(al_tab, tab_bus, al_tab_nset)
                        if elset_S1:
                            fidw.write('*Elset, elset=_tab-bus_slave_S1, internal, instance="AL TAB-1" \n')
                            if len(elset_S1)>15:
                                elset_split = [elset_S1[x:x+15] for x in range(0,len(elset_S1),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S1))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-bus_slave_S1, S1" + "\n"
                        if elset_S2:
                            fidw.write('*Elset, elset=_tab-bus_slave_S2, internal, instance="AL TAB-1" \n')
                            if len(elset_S2)>15:
                                elset_split = [elset_S2[x:x+15] for x in range(0,len(elset_S2),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S2))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-bus_slave_S2, S2" + "\n"
                        if elset_S3:
                            fidw.write('*Elset, elset=_tab-bus_slave_S3, internal, instance="AL TAB-1" \n')
                            if len(elset_S3)>15:
                                elset_split = [elset_S3[x:x+15] for x in range(0,len(elset_S3),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S3))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-bus_slave_S3, S3" + "\n"
                        if elset_S4:
                            fidw.write('*Elset, elset=_tab-bus_slave_S4, internal, instance="AL TAB-1" \n')
                            if len(elset_S4)>15:
                                elset_split = [elset_S4[x:x+15] for x in range(0,len(elset_S4),15)]
                                for e in elset_split:
                                    st = ', '.join(map(str,e))
                                    fidw.write(st)
                                    fidw.write("\n")
                            else:
                                st = ', '.join(map(str,elset_S4))
                                fidw.write(st)
                                fidw.write("\n")
                            str_slave += "_tab-bus_slave_S4, S4" + "\n"                
                        fidw.write("*Surface, type=ELEMENT, name=tab-bus_slave \n")
                        fidw.write(str_slave)

                        for line in fidm:
                            if re.findall('^\s*[0-9]', line) or "tab-bus_slave" in line:
                                continue
                            else:   break
                    
                    fidw.write(line)

                    if line.startswith("** MATERIALS"): break

            else:
                fidw.write(line)
                fidw.write("\n")
    
    os.remove(path_modify)
    os.rename("Temp.inp", path_modify)               

def define_slave_elset(elset,slave_nset,nset):
    elset_slave = []

    for m, valm in enumerate(slave_nset):
        for n, valn in enumerate(nset):
            if valm==valn[0]:
                slave_nset[m] = valn[1]

    e = [k for k in elset if any(l in slave_nset for l in k)]
    for o in e:
        surf = 1
        o.append(o[1])
        for f, s in zip(o[1:], o[2:]):
            if (f in slave_nset) & (s in slave_nset):
                elset_slave.append([o[0], surf])
                break
            surf += 1

    elset_S1, elset_S2, elset_S3, elset_S4 = ([] for i in range(4))
    for ele in elset_slave:
        if ele[1]==1:
            elset_S1.append(int(ele[0]))
        elif ele[1]==2:
            elset_S2.append(int(ele[0]))
        elif ele[1]==3:
            elset_S3.append(int(ele[0]))
        elif ele[1]==4:
            elset_S4.append(int(ele[0]))

    return elset_S1, elset_S2, elset_S3, elset_S4
