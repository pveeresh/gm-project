import matplotlib.pyplot as plt
import re
import matplotlib as mpl
import scipy.integrate as integrate

def plot_ALLJD():
    x, y, xx, i, v, x_2, y_2 = ([] for i in range(7))
    with open("abaqus_ALLJD_task1.rpt") as fid:
        for line in fid:           
           if re.findall('^\s*[0-9]', line):
                z = list(map(str,line.split()))
                z = list(map(float,z))
                x.append(z[0])
                y.append(z[1]/1000) # from mJ to J

    P_s = [(a-b)*1e3 for a,b in zip(y[1:],y)]

    with open("ALLJD_sample3_task2.txt") as fid:
        for line in fid:           
           if re.findall('^\s*[0-9]', line):
                z = list(map(str,line.split()))
                z = list(map(float,z))
                x_2.append(z[1])
                y_2.append(z[0]/1000) # from mJ to J

    P_s_2 = [(a-b)*1e3 for a,b in zip(y_2[1:],y_2)]

    c = 0
    with open("Sample#3_V.dta") as fidv:
        for line in fidv:
            if c > 17000: break
            v.append(abs(float(line)))
            xx.append(0.008*c) # ms
            c += 1

    c = 0
    with open("Sample#3_I.dta") as fidi:
        for line in fidi:
            if c > 17000: break
            i.append(float(line)*1000) # from kA to A
            c += 1
    
    P = [(a*b) for a,b in zip(i,v)]
    yy = integrate.cumtrapz(P, xx, initial=0) # cumulatively integrate using trapezoidal integration
    yy = [z/1000 for z in yy] # from mJ to J

    #yy = [sum(P[:x])*(8/1e6) for x in range(0,len(P))] # cumulatively integrate using right Riemann sum

    mpl.rcParams["font.size"] = 18

    '''fig, ax1 = plt.subplots()
    ax1.plot(x, y, marker='o', color='b', label='Voltage')
    ax1.set_xlabel("Time (ms)", fontsize=18)
    ax1.set_ylabel("Voltage (V)", fontsize=18)
    
    ax2 = ax1.twinx()
    ax2.set_ylabel("Current (kA)", fontsize=18)
    ax2.plot(xx, yy, marker='o', color='r', label='Current')
    ax2.tick_params(axis='y', labelcolor='r')

    fig.tight_layout()'''

    '''plt.plot(x, y, color='b', label='Simulation')
    plt.plot(xx, yy, color='r', label='Experimental')
    plt.ylabel("Energy (J)")
    plt.xlabel("Time (ms)")
    plt.legend()
    plt.show()'''
 
    plt.plot(x[:135], P_s[:135], color='b', label='Simulation_Task1')
    plt.plot(x_2[1:135], P_s_2[:135], color='g', label='Simulation_Task2')
    #plt.plot(xx, P, color='r', label='Experimental')
    plt.ylabel("Power (W)")
    plt.xlabel("Time (ms)")
    plt.legend()
    plt.show()


if __name__=="__main__":
    plot_ALLJD()