#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
import os
import shutil
from write_CPRESS import *
from get_slave_nodes import *
from update_geometry import *
from read_elset import *
from update_contact import *
from update_CLOAD import *
#from update_CLOAD_amp import *
from update_NT import *
from modify_current_input import *
from update_disp import *
import math

if __name__=="__main__":  
    #os.system("abaqus job=Mechanical_0 cpus=8 interactive ask_delete=OFF")
    write_CPRESS("Mechanical_0.dat")
    #update_slave_surfaces("Mechanical_0.dat","Electrical_thermal_1.inp")
       
    for ts in range(2,136):
        #if ts>1:
            #shutil.copy(f"Electrical_thermal_{ts-1}.inp",f"Electrical_thermal_{ts}.inp")           
            #update_elset(f"Mechanical_{ts-2}.dat", f"Mechanical_{ts-1}.dat", f"Electrical_thermal_{ts}.inp")
            #update_slave_surfaces(f"Mechanical_{ts-1}.dat", f"Electrical_thermal_{ts}.inp")
            #update_initial_temp_ET(f"Electrical_thermal_{ts}.inp",ts-1)
        #update_CLOAD(f"Electrical_thermal_{ts}.inp",ts)
        os.system(f"abaqus job=Electrical_thermal_{ts} user=sub_contact.for standard_parallel=solver cpus=8 interactive ask_delete=OFF")
        exit()
        #if ts>1:
            #shutil.copy(f"Mechanical_{ts-1}.inp",f"Mechanical_{ts}.inp")
            #update_initial_temp_Mech(f"Mechanical_{ts}.inp",ts)
        #os.system(f"abaqus job=Mechanical_{ts} oldjob=Mechanical_{ts-1} cpus=8 interactive ask_delete=OFF")
        #write_CPRESS(f"Mechanical_{ts}.dat")
        
