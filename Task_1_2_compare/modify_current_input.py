#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
import matplotlib.pyplot as plt
import os
import math

def modify_current_input(t,del_t):
    max = 0
    ts_start = int(t*125)
    ts_end = int(math.ceil((t+del_t)*125))
    c = ts_start
    with open("Sample#3_I.dta") as fid:
        for line in fid.readlines()[ts_start:ts_end:]:
            x = float(line)
            x = ((x/87.3)*1000)
            if x>max: max=x          
            if (c*0.008>(t+del_t)): break
            c += 1  

    c = ts_start
    i = 0
    with open("Sample#3_I.dta") as fid, open("current_load.dat", "w+") as fidw:
        for line in fid.readlines()[ts_start:ts_end:]:
            line = line.rstrip()
            line = line.lstrip()
            x = float(line)
            x = ((x/87.3)*1000)/max         
            x = format(x, '0.3f')
            line = str(round(i*0.008,3)) + ", " + x + ", "
            fidw.write(line)
            fidw.write("\n")          
            if (c*0.008>(t+del_t)): break
            c += 1
            i += 1

    with open("current_load.dat", 'rb+') as fidt:
        fidt.seek(-4, os.SEEK_END)
        fidt.truncate() 
    return round(max,3)     