import sys
import re

def read_elset(path_read):
    nset = []
    elset = []
           
    with open(path_read) as fid:
        for line in fid:
            line = line.rstrip()
            if line=="*Node":
                for line in fid:
                    line = line.rstrip()
                    line = line.lstrip()
                    
                    if line.startswith("*Element,"): break
                    
                    y = list(map(str,line.split(",")))
                    nset.append(list(map(float,y)))
                    
            if line.startswith("*Element,"):
                for line in fid:
                    line = line.rstrip()
                    line = line.lstrip()

                    if line.startswith("*Nset"): break

                    if line.startswith("*Element,"): continue

                    z = list(map(str,line.split(",")))                       
                    elset.append(list(map(float,z)))

                    if line.startswith("*System"): break
    
    return elset             