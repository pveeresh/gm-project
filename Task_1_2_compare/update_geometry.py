import sys
import re
import os
from update_disp import *

def update_elset(path_read_prev,path_read_curr,path_modify):
    node_r = calculate_disp(path_read_prev,path_read_curr)

    elset = []
    i = 0              
    with open(path_modify) as fidm, open("Temp.inp", "w+") as fidw:
        for line_m in fidm:
            line_m = line_m.rstrip()
            if line_m=="*Node":
                fidw.write(line_m)
                fidw.write("\n")
                for line_m in fidm:
                    line_m = line_m.rstrip()
                    line_m = line_m.lstrip()
                    
                    if line_m.startswith("*Element,"): break
                    
                    y = list(map(str,line_m.split(",")))
                    node_m = list(map(float,y)) 
                    node_m[0] = int(node_m[0])

                    if node_r[i][0]==node_m[0]:                 
                        node_m[1] += node_r[i][1]
                        node_m[2] += node_r[i][2]                      
                        node_m[1] = '%1.9f' % node_m[1]
                        node_m[2] = '%1.9f' % node_m[2]
                        st = ', '.join(map(str,node_m))
                        fidw.write(st)
                        fidw.write("\n")
                        i += 1

            if line_m.startswith("*Element,"):
                fidw.write(line_m)
                fidw.write("\n")
                for line_m in fidm:
                    line_m = line_m.rstrip()
                    line_m = line_m.lstrip()

                    if line_m.startswith("*Nset"): 
                        fidw.write(line_m)
                        fidw.write("\n")
                        break

                    if line_m.startswith("*Element,"): 
                        fidw.write(line_m)
                        fidw.write("\n")
                        continue

                    z = list(map(str,line_m.split(",")))                       
                    elset.append(list(map(float,z)))
                    fidw.write(line_m)
                    fidw.write("\n")

                    if line_m.startswith("*System"): break

            else:
                fidw.write(line_m)
                fidw.write("\n")
    
    os.remove(path_modify)
    os.rename("Temp.inp", path_modify)
    