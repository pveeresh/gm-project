import re
import sys
import os
import linecache
import math

def update_CLOAD(path,t):
    cread = linecache.getline("Sample#3_I.dta",math.ceil(t*125))
    cread = cread.rstrip()
    cload = float(cread)
    cload = (cload/87.3)*1000

    '''sum = 0
    i = 0
    ts_start = int(t*125)
    ts_end = int(math.ceil((t+del_t)*125))
    c = ts_start
    with open("Sample#3_I.dta") as fid:
        for line in fid.readlines()[ts_start:ts_end:]:
            x = float(line)
            x = ((x/87.3)*1000)
            sum += x
            i += 1         
            if (c*0.008>(t+del_t)): break
            c += 1

    cload = sum/i'''

    with open(path) as fidm, open("Temp.inp", "w+") as fidw:
        for line in fidm:
            line = line.rstrip()
            line = line.lstrip()
            
            if line.startswith("*Step, name=Electrical_thermal"):
                fidw.write(line)
                fidw.write("\n")
                for line in fidm:
                    line = line.rstrip()
                    if line=="** LOADS": 
                        fidw.write(line)
                        fidw.write("\n")
                        break
                    if line.startswith("*"):
                        fidw.write(line)
                        fidw.write("\n")
                        continue
                    st = "0.05, 1., 0.05, 1.,"
                    fidw.write(st)
                    fidw.write("\n")
            
            elif line.startswith("Current_load"):
                st = ''.join(["Current_load,CS,",str(round(cload,3))])
                fidw.write(st)
                fidw.write("\n")

            else:
                fidw.write(line)
                fidw.write("\n")
    
    os.remove(path)
    os.rename("Temp.inp", path) 