      program main
       call gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1  msname,coords,noel,node,npred,kstep,kinc)

       call gapelectr(sigma,d,temp,predef,time,ciname,slname,
     1  msname,coords,node,npred,kstep,kinc)
      end program main

      subroutine read(nn,cpress)
       dimension nn(300),cpress(300)

       open(unit=66, file="c:\\temp\\Task_1_2_compare\\cpress.dat", iostat=ios)
       if (ios /= 0) stop "error"

       n = 0
       do
        read(66,'(a)', iostat=ios) line
        if (ios /= 0) exit
        n = n + 1
       end do

       rewind(66)

       do i = 1, n
        read(66,'(i5,f15.10)') nn(i),cpress(i)
       end do  
       close(66)

       return
      end subroutine read

      subroutine gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1 msname,coords,noel,node,npred,kstep,kinc)

       include 'aba_param_dp.inc'

       character*80 ciname,slname,msname
       real cpress
       integer nn

       dimension ak(5),d(2),flowm(2),temp(2),predef(2,*),nn(300),
     1  cpress(300),coords(2,2)  

       call read(nn,cpress)

       do i = 1, size(nn)
         if (nn(i) .gt. 14000) then
           nn(i) = nn(i) + 522
         end if
       end do

       do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
       end do
       
       p = d(2)
      
       if (ciname == 'CU-CU') then
        ecr = 3*0.05*(210.74/p)*(0.0000250+0.0000168)*0.5

       else if (ciname == 'CU-AL') then
        ecr = 1.93E-8*p**(-2.0/3.0) - 
     1   8.1E-9*p**(-0.5) + 1.25E-9*p**(-1.0/3.0)
        ecr = ecr*1.0E6

       end if

       tcr = ecr/(0.0000000244*(25+273))
       ak(1) = 1.0/tcr

c      write(*,77) 'error_t',d(2),node,ak(1)
 77    format (a, f7.3, i, f7.3 /)

      return
      end subroutine gapcon

      subroutine gapelectr(sigma,d,temp,predef,time,ciname,
     1 slname,msname,coords,node,npred,kstep,kinc)

      include 'aba_param_dp.inc'

      character*80 ciname,slname,msname
      real cpress
      integer nn

      dimension sigma(5),d(2),temp(2),predef(2,*),time(2),
     1 coords(2,2),nn(300),cpress(300)

      call read(nn,cpress)

      do i = 1, size(nn)
        if (nn(i) .gt. 14000) then
          nn(i) = nn(i) + 522
        end if
      end do

      do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
      end do

      p = d(2)
      
      if (ciname == 'CU-CU') then
       ecr = 3*0.05*(210.74/p)*(0.0000250+0.0000168)*0.5

      else if (ciname == 'CU-AL') then
        ecr = 1.93E-8*p**(-2.0/3.0) - 
     1   8.1E-9*p**(-0.5) + 1.25E-9*p**(-1.0/3.0)
        ecr = ecr*1.0E6

      end if

      sigma(1) = 1.0/ecr

c     open (unit=1, file="c:\\temp\\Task_1_2_compare\\test.dat")
c     write(1,'(i)') node

c     write(*,88) 'error_e',d(2),node,sigma(1)
 88   format (a, f7.3, i, f20.6, /)

      return
      end subroutine gapelectr