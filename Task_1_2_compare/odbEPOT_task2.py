#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
from odbAccess import *
from sys import argv,exit

def getMaxFO():
   fid = open("EPOT_sample3_task2.txt","w")
   for i in range(1,136):
        odbname = "Electrical_thermal_%s.odb"%i
        odb = openOdb(odbname)
        FO = 'EPOT'
        max_FO = 0
        for step in odb.steps.values():     
            frame = step.frames[-1]        
            allFields = frame.fieldOutputs 
            nodeset = odb.rootAssembly.instances['PART-1-1'].nodeSets['Top electrode']
            if (allFields.has_key(FO)):
                FOSet = allFields[FO]
                FOSet = FOSet.getSubset(region=nodeset)  
                for FOValue in FOSet.values:
                    if FOValue.data>max_FO: max_FO=FOValue.data

        fid.write(str(round(max_FO,5)) + " " + str(i))
        fid.write("\n")
        odb.close()

if __name__ == '__main__':
    getMaxFO()