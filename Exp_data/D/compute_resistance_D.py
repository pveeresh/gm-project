import sys
import re
import scipy.integrate as integrate
import matplotlib.pyplot as plt

'''Computes pulse-wise average resistance for every sample i.e. three resistances per sample. Average resistance calculated using IV/I**2'''
def getIV(path_I, path_V):
    I, V = ([] for i in range(2))

    with open(path_I) as fid:
        for line in fid:
            z = float(line)
            I.append(z*1000)
                      
    with open(path_V) as fid:
        for line in fid:
            z = float(line)
            V.append(z)

    return I, V

def findR():
    u, R_avg, error = ([] for i in range(3))
    paths = ["C:/temp/Expdata/D/I/_532.dta", "C:/temp/Expdata/D/V/_532.dta", "C:/temp/Expdata/D/I/_533.dta", "C:/temp/Expdata/D/V/_533.dta", "C:/temp/Expdata/D/I/_534.dta", "C:/temp/Expdata/D/V/_534.dta",
             "C:/temp/Expdata/D/I/_535.dta", "C:/temp/Expdata/D/V/_535.dta", "C:/temp/Expdata/D/I/_536.dta", "C:/temp/Expdata/D/V/_536.dta"]
    cols = ['D_P1','D_P2','D_P3']
    index = range(len(cols))

    for j in range(0,len(paths),2):
        I, V = getIV(paths[j],paths[j+1])
        n = [0,1250,2500,3750]
        for i in range(3):
            P_IV = [(a*b) for a,b in zip(I[n[i]:n[i+1]],V[n[i]:n[i+1]])]
            P_IV_int = integrate.trapz(P_IV, dx=0.008)

            P_I2 = [a*a for a in I[n[i]:n[i+1]]]
            P_I2_int = integrate.trapz(P_I2, dx=0.008)

            R_avg.append((P_IV_int/P_I2_int)*1e6)

    for k in range(len(cols)):
        l = len(R_avg[k:-3:3])
        u.append(sum(R_avg[k:-3:3])/l)
        s = (sum((a-u[k])**2 for a in R_avg[k:-3:3])/(l-1))**0.5
        error.append((2.015*s)/(l**0.5))

    plt.rcParams["font.size"] = 28
    fig, ax = plt.subplots()
    plt.bar(index, u, yerr=error, capsize=10, color=['b','g','r'])
    plt.ylabel("Resistance (micro-ohm)")
    ax.set_xticklabels(cols)
    ax.set_xticks(index)
    ax.set_ylim([0,840])
    # style = dict(color='red')
    # for i in range(len(cols)):
    #     ax.text(index[i],u[i],round(u[i],1),ha='center',**style)
    plt.show()
    
if __name__=="__main__":
    findR()