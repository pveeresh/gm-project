from __future__ import division
import sys
import re
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from matplotlib.pyplot import cm
import math
import numpy as np

'''Computes average resistance point-wise across all samples to get average ECR vs time. Linear fit for all on-cycle.
   Connect ends of on cycles with linear fit to get off cycles. Constant ECR before first and after last on cycle.'''
def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

def getIV(path_I, path_V):
    I, V = ([] for i in range(2))

    d = []
    with open(path_I) as fid:
        for c, line in enumerate(fid):
            z = float(line)
            if (z*1000)<=0.0: 
                d.append(c)
                I.append(1.0)
                continue
            I.append(z*1000)    
            if c*0.008>=30: break
            
    with open(path_V) as fid:
        for c, line in enumerate(fid):
            z = float(line)
            if any(c==a for a in d) or z<0: 
                V.append(0)
                continue
            V.append(z)
            if c*0.008>=30: break

            
    return I, V

def findR():
    R_B, R_D, R_B_avg, R_D_avg, t, s_D, error_D = ([] for i in range(7))
    paths_B = ["C:/temp/Expdata/B/I/_526.dta", "C:/temp/Expdata/B/V/_526.dta", "C:/temp/Expdata/B/I/_527.dta", "C:/temp/Expdata/B/V/_527.dta", "C:/temp/Expdata/B/I/_528.dta", "C:/temp/Expdata/B/V/_528.dta"]
    paths_D = ["C:/temp/Expdata/D/I/_532.dta", "C:/temp/Expdata/D/V/_532.dta", "C:/temp/Expdata/D/I/_533.dta", "C:/temp/Expdata/D/V/_533.dta", "C:/temp/Expdata/D/I/_534.dta", "C:/temp/Expdata/D/V/_534.dta",
               "C:/temp/Expdata/D/I/_535.dta", "C:/temp/Expdata/D/V/_535.dta", "C:/temp/Expdata/D/I/_536.dta", "C:/temp/Expdata/D/V/_536.dta"]
    mpl.rcParams["font.size"] = 18
    color = iter(cm.rainbow(np.linspace(0,0.8,5)))
    for i in frange(0, 30.0, 0.008):
        t.append(i)

    for j in range(0,len(paths_B),2):
        I, V = getIV(paths_B[j],paths_B[j+1])
        R_B.append([y/x for x, y in zip(I, V)])

    for k in range(len(R_B[0])):
        R_B_avg.append(sum(R_B[z][k] for z in range(3))/3)


    for j in range(0,len(paths_D),2):
        I, V = getIV(paths_D[j], paths_D[j+1])
        R_D.append([y/x for x, y in zip(I, V)])

    l_D = 5
    for k in range(len(R_D[0])):
        R_D_avg.append(sum(R_D[z][k] for z in range(l_D))/l_D)
        s_D.append((sum((R_D[a][k]-R_D_avg[k])**2 for a in range(l_D))/(l_D-1))**0.5)
        error_D.append((2.015*s_D[k])/(l_D**0.5))


    R = [0.5*(b-a-4.63e-9-2.94e-9)*(math.pi*2.54**2) for a,b in zip(R_B_avg, R_D_avg)]
    
    # Pulse-1 - on
    coef1 = np.polyfit(t[math.ceil(2.25*125):math.ceil(8.25*125)],R[math.ceil(2.25*125):math.ceil(8.25*125)],1)
    fn1 = np.poly1d(coef1)
    print(coef1)

    # Pulse-2 - on
    coef2 = np.polyfit(t[math.ceil(12.25*125):math.ceil(18.25*125)],R[math.ceil(12.25*125):math.ceil(18.25*125)],1)
    fn2 = np.poly1d(coef2)
    print(coef2)

    # off cycle b/w pulse-1 and pulse-2
    coef12 = np.polyfit([t[math.ceil(8.25*125)-1],t[math.ceil(12.25*125)]],[fn1(t[math.ceil(8.25*125)-1]),fn2(t[math.ceil(12.25*125)])],1)
    fn12 = np.poly1d(coef12)
    print(coef12)

    # Pulse-3 - on
    coef3 = np.polyfit(t[math.ceil(22.25*125):math.ceil(28.25*125)],R[math.ceil(22.25*125):math.ceil(28.25*125)],1)
    fn3 = np.poly1d(coef3)
    print(coef3)
  
    # off cycle b/w pulse-2 and pulse-3
    coef23 = np.polyfit([t[math.ceil(18.25*125)-1],t[math.ceil(22.25*125)]],[fn2(t[math.ceil(18.25*125)-1]),fn3(t[math.ceil(22.25*125)])],1)
    fn23 = np.poly1d(coef23)
    print(coef23)

    plt.rcParams["font.size"] = 18
    fig, ax = plt.subplots()
    
    #plt.fill_between(t, [x-y for x,y in zip(R_D_avg,error_D)], [x+y for x,y in zip(R_D_avg,error_D)], color='#377EB8', alpha=0.3)
    #plt.scatter(t, R_D_avg, s=10)

    plt.scatter(t, R, s=10, label='Average contact resistance')
    plt.plot(t[0:math.ceil(2.25*125)+1],[fn1(t[math.ceil(2.25*125)]) for i in range(len(t[0:math.ceil(2.25*125)+1]))],'--r', linewidth=3)
    plt.plot(t[math.ceil(2.25*125):math.ceil(8.25*125)],fn1(t[math.ceil(2.25*125):math.ceil(8.25*125)]),'--r', linewidth=3, label='Fit')
    plt.plot([t[math.ceil(8.25*125)-1],t[math.ceil(12.25*125)]],fn12([t[math.ceil(8.25*125-1)],t[math.ceil(12.25*125)]]),'--r', linewidth=3)
    plt.plot(t[math.ceil(12.25*125):math.ceil(18.25*125)],fn2(t[math.ceil(12.25*125):math.ceil(18.25*125)]),'--r', linewidth=3)
    plt.plot([t[math.ceil(18.25*125)-1],t[math.ceil(22.25*125)]],fn23([t[math.ceil(18.25*125-1)],t[math.ceil(22.25*125)]]),'--r', linewidth=3)
    plt.plot(t[math.ceil(22.25*125):math.ceil(28.25*125)],fn3(t[math.ceil(22.25*125):math.ceil(28.25*125)]),'--r', linewidth=3)
    plt.plot(t[math.ceil(28.25*125)-1:30*125+1],[fn3(t[math.ceil(28.25*125)-1]) for i in range(len(t[math.ceil(28.25*125)-1:30*125+1]))],'--r', linewidth=3)
    #plt.ylabel(r'Resistance ($\Omega$)')    
    plt.ylabel(r'ECR$_{Cu/Al}$ ($\Omega$mm$^2$)')
    plt.xlabel(r'Time (ms)') 
    ax.set_ylim([0,1e-2])
    ax.margins(x=0) 
    plt.locator_params(axis='y', nbins=5)
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax.tick_params(direction="in")
    plt.legend()
    plt.show()
    
if __name__=="__main__":
    findR()