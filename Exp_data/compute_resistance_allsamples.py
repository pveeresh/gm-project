import sys
import re
import scipy.integrate as integrate
import matplotlib.pyplot as plt

'''Computes pulse-wise average resistance across all samples. Average resistance calculated using IV/I**2'''
def getIV(path_I, path_V):
    I, V = ([] for i in range(2))

    with open(path_I) as fid:
        for line in fid:
            z = float(line)
            I.append(z*1000)
                      
    with open(path_V) as fid:
        for line in fid:
            z = float(line)
            V.append(z)

    return I, V

def findR():
    R_avg, error = ([] for i in range(2))
    paths = ["C:/temp/Expdata/D/I/_532.dta", "C:/temp/Expdata/D/V/_532.dta", "C:/temp/Expdata/D/I/_533.dta", "C:/temp/Expdata/D/V/_533.dta", "C:/temp/Expdata/D/I/_534.dta", "C:/temp/Expdata/D/V/_534.dta",
             "C:/temp/Expdata/D/I/_535.dta", "C:/temp/Expdata/D/V/_535.dta", "C:/temp/Expdata/D/I/_536.dta", "C:/temp/Expdata/D/V/_536.dta", "C:/temp/Expdata/D/I/_537.dta", "C:/temp/Expdata/D/V/_537.dta"]    
    cols = ['D_S1_P1','D_S1_P2','D_S1_P3','D_S2_P1','D_S2_P2','D_S2_P3','D_S3_P1','D_S3_P2','D_S3_P3','D_S4_P1','D_S4_P2','D_S4_P3','D_S5_P1','D_S5_P2','D_S5_P3','D_S6_P1','D_S6_P2','D_S6_P3']
    index = range(len(cols))

    for j in range(0,len(paths),2):
        I, V = getIV(paths[j],paths[j+1])
        n = [0,1250,2500,3750] #125 sample points per ms; each cycle lasts 10 ms
        for i in range(3):
            P_IV = [(a*b) for a,b in zip(I[n[i]:n[i+1]],V[n[i]:n[i+1]])]
            P_IV_int = integrate.trapz(P_IV, dx=0.008)

            l = len(P_IV)
            u = sum(P_IV)/l
            s = (sum((a-u)**2 for a in P_IV)/(l-1))**0.5
            error.append((1.645*s)/(l**0.5))

            P_I2 = [a*a for a in I[n[i]:n[i+1]]]
            P_I2_int = integrate.trapz(P_I2, dx=0.008)

            R_avg.append((P_IV_int/P_I2_int)*1e6)

    plt.rcParams["font.size"] = 14
    fig, ax = plt.subplots()
    plt.bar(index, R_avg, color=['royalblue','royalblue','royalblue','salmon','salmon','salmon','olive','olive','olive','gray','gray','gray','brown','brown','brown','g','g','g'])
    plt.ylabel("Resistance (micro-ohm)")
    ax.set_xticklabels(cols)
    ax.set_xticks(index)
    ax.tick_params(axis='x', labelsize=8)
    style = dict(color='red')
    for i in range(len(cols)):
        ax.text(index[i],R_avg[i],round(R_avg[i],1),ha='center',**style)
    plt.show()
    
if __name__=="__main__":
    findR()