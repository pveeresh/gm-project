from __future__ import division
import sys
import re
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import math
import numpy as np

'''Computes average resistance point-wise across all samples to get average ECR vs time. Linear fit for all on-cycle.
   Connect ends of on cycles with linear fit to get off cycles. Constant ECR before first and after last on cycle.'''
def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

def getIV(path_I, path_V):
    I, V = ([] for i in range(2))

    with open(path_I) as fid:
        for c, line in enumerate(fid):
            z = float(line)
            I.append(z*1000)
            if c*0.008>=30: break
            
    
    with open(path_V) as fid:
        for c, line in enumerate(fid):
            z = float(line)
            V.append(z)
            if c*0.008>=30: break
            
    return I, V

def findR():
    I_B, V_B, R_B, R_BB, R_B_avg, R_BB_avg, t, s_B, error_B, s_BB, error_BB = ([] for i in range(11))
    paths_B = ["C:/temp/Expdata/B/I/_526.dta", "C:/temp/Expdata/B/V/_526.dta", "C:/temp/Expdata/B/I/_527.dta", "C:/temp/Expdata/B/V/_527.dta", "C:/temp/Expdata/B/I/_528.dta", "C:/temp/Expdata/B/V/_528.dta"]
    paths_BB = ["C:/temp/Expdata/B_clampone/I/_523.dta", "C:/temp/Expdata/B_clampone/V/_523.dta", "C:/temp/Expdata/B_clampone/I/_525.dta", "C:/temp/Expdata/B_clampone/V/_525.dta"]

    for j in range(0,len(paths_B),2):
        I, V = getIV(paths_B[j],paths_B[j+1])
        R_B.append([y/x for x, y in zip(I, V)])

    l_B = 3
    for k in range(len(R_B[0])):
        R_B_avg.append(sum(R_B[z][k] for z in range(l_B))/l_B)
        s_B.append((sum((R_B[a][k]-R_B_avg[k])**2 for a in range(l_B))/(l_B-1))**0.5)
        error_B.append((2.92*s_B[k])/(l_B**0.5))

    for j in range(0,len(paths_BB),2):
        I, V = getIV(paths_BB[j],paths_BB[j+1])
        R_BB.append([y/x for x, y in zip(I, V)])

    l_BB = 2
    for k in range(len(R_BB[0])):
        R_BB_avg.append(sum(R_BB[z][k] for z in range(2))/2)
        s_BB.append((sum((R_BB[a][k]-R_BB_avg[k])**2 for a in range(l_BB))/(l_BB-1))**0.5)
        error_BB.append((6.314*s_BB[k])/(l_BB**0.5))


    for i in frange(0, 30.0, 0.008):
        t.append(i)

    R = [(a-b-2.25e-6)*(math.pi*2.54**2) for a,b in zip(R_B_avg, R_BB_avg)]
    
    # Pulse-1 - on
    coef1 = np.polyfit(t[2*125:8*125],R[2*125:8*125],1)
    fn1 = np.poly1d(coef1)
    print(coef1)

    # Pulse-2 - on
    coef2 = np.polyfit(t[12*125:18*125],R[12*125:18*125],1)
    fn2 = np.poly1d(coef2)
    print(coef2)

    # off cycle b/w pulse-1 and pulse-2
    coef12 = np.polyfit([t[8*125-1],t[12*125]],[fn1(t[8*125-1]),fn2(t[12*125])],1)
    fn12 = np.poly1d(coef12)
    print(coef12)

    # Pulse-3 - on
    coef3 = np.polyfit(t[22*125:28*125],R[22*125:28*125],1)
    fn3 = np.poly1d(coef3)
    print(coef3)

    # off cycle b/w pulse-2 and pulse-3
    coef23 = np.polyfit([t[18*125-1],t[22*125]],[fn2(t[18*125-1]),fn3(t[22*125])],1)
    fn23 = np.poly1d(coef23)
    print(coef23)

    plt.rcParams["font.size"] = 18
    fig, ax = plt.subplots()
    
    #plt.fill_between(t, [x-y for x,y in zip(R_BB_avg,error_BB)], [x+y for x,y in zip(R_BB_avg,error_BB)], color='#377EB8', alpha=0.3)
    #plt.scatter(t, R_BB_avg, c='#377EB8', s=10)
    
    plt.scatter(t, R, s=10, label='Average contact resistance')
    plt.plot(t[0:2*125+1],[fn1(t[2*125]) for i in range(len(t[0:2*125+1]))],'--r', linewidth=3)
    plt.plot(t[2*125:8*125],fn1(t[2*125:8*125]),'--r', linewidth=3, label='Fit')
    plt.plot([t[8*125-1],t[12*125]],fn12([t[8*125-1],t[12*125]]),'--r', linewidth=3)
    plt.plot(t[12*125:18*125],fn2(t[12*125:18*125]),'--r', linewidth=3)
    plt.plot([t[18*125-1],t[22*125]],fn23([t[18*125-1],t[22*125]]),'--r', linewidth=3)
    plt.plot(t[22*125:28*125],fn3(t[22*125:28*125]),'--r', linewidth=3)
    plt.plot(t[28*125-1:30*125+1],[fn3(t[28*125-1]) for i in range(len(t[28*125-1:30*125+1]))],'--r', linewidth=3)
    plt.ylabel(r'ECR$_{Cu/CuZr}$ ($\Omega$mm$^2$)')
    #plt.ylabel(r'Resistance ($\Omega$)') 
    plt.xlabel(r'Time (ms)') 
    ax.set_ylim([0,0.0035])
    #ax.set_ylim([0,4E-4])
    ax.margins(x=0)
    plt.locator_params(axis='y', nbins=5)
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax.tick_params(direction="in")
    plt.legend()
    plt.show()
    
if __name__=="__main__":
    findR()