import re
import sys
import os
import linecache

def update_initial_temp_Mech(path,ts):
    with open(path) as fidm, open("Temp.inp", "w+") as fidw:
        for line in fidm:
            line = line.rstrip()
            if line.startswith("*TEMPERATURE"):
                st = f"*TEMPERATURE, file=C:/Temp/Task_2_contact/Electrical_thermal_{ts}.fil"
                fidw.write(st)
                fidw.write("\n")

            else:
                fidw.write(line)
                fidw.write("\n")
    
    os.remove(path)
    os.rename("Temp.inp", path)


def update_initial_temp_ET(path,ts):
 
    with open(path) as fidm, open("Temp.inp", "w+") as fidw:
        for line in fidm:
            line = line.rstrip()
            if line.startswith("** Name: Initial temp"):
                fidw.write(line)
                fidw.write("\n")
                for line in fidm:
                    if line.startswith("*Initial Conditions"): 
                        st = f"*Initial Conditions, type=TEMPERATURE, file=C:/Temp/Task_2_contact/Electrical_thermal_{ts}.fil"
                        fidw.write(st)
                        fidw.write("\n")
                    if line.startswith("**"): 
                        fidw.write(line)
                        break
                                             
            else:
                fidw.write(line)
                fidw.write("\n")
    os.remove(path)
    os.rename("Temp.inp", path)