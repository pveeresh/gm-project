      program main
       call gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1  msname,coords,noel,node,npred,kstep,kinc)

       call gapelectr(sigma,d,temp,predef,time,ciname,slname,
     1  msname,coords,node,npred,kstep,kinc)
      end program main

      subroutine read(nn,cpress)
       dimension nn(300),cpress(300)

       open(unit=66, file="c:\\temp\\Task_2_contact\\cpress.dat",
     1  iostat=ios)
       if (ios /= 0) stop "error reading cpress"

       n = 0
       do
        read(66,'(a)', iostat=ios) line
        if (ios /= 0) exit
        n = n + 1
       end do

       rewind(66)

       do i = 1, n
        read(66,'(i5,f15.10)') nn(i),cpress(i)
       end do  
       close(66)

       return
      end subroutine read

      subroutine gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1 msname,coords,noel,node,npred,kstep,kinc)

       include 'aba_param_dp.inc'

       character*80 ciname,slname,msname
       real cpress
       integer nn

       dimension ak(5),d(2),flowm(2),temp(2),predef(2,*),nn(300),
     1  cpress(300),coords(2,2)  

       call read(nn,cpress)

       do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
       end do

       if (temp(1) == 0.0) then
        temp(1) = 25.0
       end if
       if (temp(2) == 0.0) then
        temp(2) = 25.0
       end if
       
       p = d(2)
       t = max(temp(1),temp(2))
      
       if (ciname == 'CU-CU') then
         if ((t .gt. 0.0) .and. (t .le. 25.0)) then
          ecr = 3*0.05*(209.83/p)*(1.71E-5+2.54E-5)*0.5
         else if ((t .gt. 25.0) .and. (t .le. 100.0)) then
          ecr = 3*0.05*(206.28/p)*(2.19E-5+3.10E-5)*0.5
         else if ((t .gt. 100.0) .and. (t .le. 200.0)) then
          ecr = 3*0.05*(191.45/p)*(2.87E-5+4.04E-5)*0.5
         else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
          ecr = 3*0.05*(143.27/p)*(3.58E-5+5.17E-5)*0.5
         else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
          ecr = 3*0.05*(80.97/p)*(4.30E-5+6.51E-5)*0.5
         else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
          ecr = 3*0.05*(34.48/p)*(5.04E-5+8.05E-5)*0.5
         else if ((t .gt. 500.0) .and. (t .le. 600.0)) then
          ecr = 3*0.05*(26.91/p)*(5.81E-5+9.78E-5)*0.5
         else if ((t .gt. 600.0) .and. (t .le. 660.0)) then
          ecr = 3*0.05*(17.34/p)*(6.28E-5+1.09E-4)*0.5
         else if (t .gt. 660.0) then
          ecr = 3*0.05*(16.93/p)*(6.84E-5+1.23E-4)*0.5
         else
          write(*,*) 'none'
         end if

       else if (ciname == 'CU-AL') then
         ecr_20 = (1.93E-8*p**(-2.0/3.0) - 
     1    8.1E-9*p**(-0.5) + 1.25E-9*p**(-1.0/3.0))*1.0E6
 
         if ((t .gt. 0.0) .and. (t .le. 25.0)) then
          ecr = ecr_20
         else if ((t .gt. 25.0) .and. (t .le. 100.0)) then
          ecr = ecr_20*((31.85/34.1)**1.3)*((2.19E-5+3.64E-5)
     1     /(1.71E-5+2.71E-5))
         else if ((t .gt. 100.0) .and. (t .le. 200.0)) then
          ecr = ecr_20*((24.63/34.1)**1.3)*((2.87E-5+4.92E-5)
     1     /(1.71E-5+2.71E-5))
         else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
          ecr = ecr_20*((15.04/34.1)**1.3)*((3.58E-5+6.19E-5)
     1     /(1.71E-5+2.71E-5))
         else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
          ecr = ecr_20*((11.34/34.1)**1.3)*((4.30E-5+7.3E-5)
     1     /(1.71E-5+2.71E-5))
         else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
          ecr = ecr_20*((9.69/34.1)**1.3)*((5.04E-5+8.61E-5)
     1     /(1.71E-5+2.71E-5))
         else if ((t .gt. 500.0) .and. (t .le. 600.0)) then
          ecr = ecr_20*((8.32/34.1)**1.3)*((5.81E-5+9.82E-5)
     1     /(1.71E-5+2.71E-5))
         else if ((t .gt. 600.0) .and. (t .le. 760.0)) then
          ecr = ecr_20*((7.68/34.1)**1.3)*((6.28E-5+1.06E-4)
     1     /(1.71E-5+2.71E-5))
         else if (t .gt. 760.0) then
          ecr = ecr_20*((1.0/34.1)**1.3)*((6.84E-5+2.58E-4)
     1     /(1.71E-5+2.71E-5))
         else
          write(*,*) 'none'
         end if 
       end if

       if (ecr .lt. 1.06E-5) then
        ecr = 1.06E-4
       end if

       tcr = ecr/(0.0000000244*(t+273))
       ak(1) = 1.0/tcr

c      open(unit=23, file='c:\\temp\\contact_nodes.txt', access='append', status='old')
c      write(23,12) node
 12    format(i)


c      write(*,77) 'error_t',t,d(2),node,ak(1)
 77    format (a, f6.2, f7.3, i, f7.3 /)

      return
      end subroutine gapcon

      subroutine gapelectr(sigma,d,temp,predef,time,ciname,
     1 slname,msname,coords,node,npred,kstep,kinc)

      include 'aba_param_dp.inc'

      character*80 ciname,slname,msname
      real cpress
      integer nn

      dimension sigma(5),d(2),temp(2),predef(2,*),time(2),
     1 coords(2,2),nn(300),cpress(300)

      call read(nn,cpress)

      do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
      end do

      if (temp(1) == 0.0) then
       temp(1) = 25.0
      end if
      if (temp(2) == 0.0) then
       temp(2) = 25.0
      end if

      p = d(2)
      t = max(temp(1),temp(2))
      
      if (ciname == 'CU-CU') then
       if ((t .gt. 0.0) .and. (t .le. 25.0)) then
        ecr = 3*0.05*(209.83/p)*(1.71E-5+2.54E-5)*0.5
       else if ((t .gt. 25.0) .and. (t .le. 100.0)) then
        ecr = 3*0.05*(206.28/p)*(2.19E-5+3.10E-5)*0.5
       else if ((t .gt. 100.0) .and. (t .le. 200.0)) then
        ecr = 3*0.05*(191.45/p)*(2.87E-5+4.04E-5)*0.5
       else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
        ecr = 3*0.05*(143.27/p)*(3.58E-5+5.17E-5)*0.5
       else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
        ecr = 3*0.05*(80.97/p)*(4.30E-5+6.51E-5)*0.5
       else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
        ecr = 3*0.05*(34.48/p)*(5.04E-5+8.05E-5)*0.5
       else if ((t .gt. 500.0) .and. (t .le. 600.0)) then
        ecr = 3*0.05*(26.91/p)*(5.81E-5+9.78E-5)*0.5
       else if ((t .gt. 600.0) .and. (t .le. 660.0)) then
        ecr = 3*0.05*(17.34/p)*(6.28E-5+1.09E-4)*0.5
       else if (t .gt. 660.0) then
        ecr = 3*0.05*(16.93/p)*(6.84E-5+1.23E-4)*0.5
       else
        write(*,*) 'none'
       end if

      else if (ciname == 'CU-AL') then
       ecr_20 = (1.93E-8*(p**(-2.0/3.0)) - 
     1  8.1E-9*(p**(-0.5)) + 1.25E-9*(p**(-1.0/3.0)))*1.0E6

       if ((t .gt. 0.0) .and. (t .le. 25.0)) then
        ecr = ecr_20
       else if ((t .gt. 25.0) .and. (t .le. 100.0)) then
        ecr = ecr_20*((31.85/34.1)**1.3)*((2.19E-5+3.64E-5)
     1   /(1.71E-5+2.71E-5))
       else if ((t .gt. 100.0) .and. (t .le. 200.0)) then
        ecr = ecr_20*((24.63/34.1)**1.3)*((2.87E-5+4.92E-5)
     1   /(1.71E-5+2.71E-5))
       else if ((t .gt. 200.0) .and. (t .le. 300.0)) then
        ecr = ecr_20*((15.04/34.1)**1.3)*((3.58E-5+6.19E-5)
     1   /(1.71E-5+2.71E-5))
       else if ((t .gt. 300.0) .and. (t .le. 400.0)) then
        ecr = ecr_20*((11.69/34.1)**1.3)*((4.30E-5+7.3E-5)
     1   /(1.71E-5+2.71E-5))
       else if ((t .gt. 400.0) .and. (t .le. 500.0)) then
        ecr = ecr_20*((9.69/34.1)**1.3)*((5.04E-5+8.61E-5)
     1   /(1.71E-5+2.71E-5))
       else if ((t .gt. 500.0) .and. (t .le. 600.0)) then
        ecr = ecr_20*((8.32/34.1)**1.3)*((5.81E-5+9.82E-5)
     1   /(1.71E-5+2.71E-5))
       else if ((t .gt. 600.0) .and. (t .le. 760.0)) then
        ecr = ecr_20*((7.68/34.1)**1.3)*((6.28E-5+1.06E-4)
     1   /(1.71E-5+2.71E-5))
       else if (t .gt. 760.0) then
        write(*,*) "hello"
        ecr = ecr_20*((1.0/34.1)**1.3)*((6.84E-5+2.58E-4)
     1   /(1.71E-5+2.71E-5))
       else
        write(*,*) 'none'
       end if        
      end if

      if (ecr .lt. 1.06E-5) then
       ecr = 1.06E-4
      end if

      sigma(1) = 1.0/ecr

c     open(unit=23, file='contact_nodes.txt')
c     write(23,12) node
 12   format(i, /)

c     write(*,88) 'error_e',t,d(2),node,sigma(1)
 88   format (a, f6.2, f7.3, i, f20.6, /)

      return
      end subroutine gapelectr