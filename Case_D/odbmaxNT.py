from odbAccess import *
from sys import argv,exit

'''Computes average interface temperature (averaged over contact nodes) for every time step'''
def getMaxFO():
    odb = openOdb("D_TE.odb")

    FO = 'NT11'
    fid = open("NT_D.txt","w")

    for step in odb.steps.values():
        print 'Processing Step:', step.name      
        for frame in step.frames:
            max_FO = 0
            sum_FO = 0
            c = 0 
            allFields = frame.fieldOutputs
            nodeset = odb.rootAssembly.instances['PART-1-1'].nodeSets['AL_TAB_TOP_SURF']
            if (allFields.has_key(FO)):
                FOSet = allFields[FO]
                FOSet = FOSet.getSubset(region=nodeset) 
                for FOValue in FOSet.values:
                    sum_FO += FOValue.data
                    c += 1
                    if FOValue.data>max_FO: max_FO=FOValue.data

            mean_FO = sum_FO/c
            fid.write(str(round(mean_FO,5)) + " " + frame.description)
            fid.write("\n")
    odb.close()

if __name__ == '__main__':
    getMaxFO()