c     Abaqus subroutine for thermal and electrical contact resistance
      program main
       call gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1  msname,coords,noel,node,npred,kstep,kinc)

       call gapelectr(sigma,d,temp,predef,time,ciname,slname,
     1  msname,coords,node,npred,kstep,kinc)
      end program main

c 	  Reads contact pressure from abaqus output data file
      subroutine read(nn,cpress)
       dimension nn(300),cpress(300)

       open(unit=66, file="c:\\temp\\CaseD\\cpress.dat",iostat=ios)
       if (ios /= 0) stop "error reading cpress"

       n = 0
       do
        read(66,'(a)', iostat=ios) line
        if (ios /= 0) exit
        n = n + 1
       end do

       rewind(66)

       do i = 1, n
        read(66,'(i5,f15.10)') nn(i),cpress(i)
       end do  
       close(66)

       return
      end subroutine read

c     Log-fit
      subroutine fit(x,y,x_fit,y_fit,n)
       integer n, m
       real a, b, d, xx(n), yy(n)
       real a1, a2, b0, b1, x_fit, y_fit
       a1 = 0
       a2 = 0
       b0 = 0
       b1 = 0
       xx = log10(x)
       yy = log10(y)
       do m = 0,n-1
        a1 = a1 + xx(m)
        a2 = a2 + xx(m)*xx(m)
        b0 = b0 + yy(m)
        b1 = b1 + yy(m)*xx(m)
       end do
       a1 = a1/n
       a2 = a2/n
       b0 = b0/n
       b1 = b1/n
       d = a1*a1 - a2
       a = (a1*b1 - a2*b0)/d
       b = (a1*b0 - b1)/d       
       y_fit = 10**(a + b*log10(x_fit))

c       open(unit=23, file='c:\\temp\\caseb\\fit_test.txt')
c       write(*,'(i)') n
c       write(*,'(E10.3)') a,b,x_fit,y_fit,d

       return
      end subroutine fit

c     Computes and returns thermal contact resistance
      subroutine gapcon(ak,d,flowm,temp,predef,time,ciname,slname,
     1 msname,coords,noel,node,npred,kstep,kinc)

       include 'aba_param_dp.inc'

       character*80 ciname,slname,msname
       real cpress,sy_cu_temp(5),sy_cu(5),rho_temp(10),rho_cu(10),
     1  rho_cuzr(10),sy,rho_1,rho_2,tt
       integer nn

       dimension ak(5),d(2),flowm(2),temp(2),predef(2,*),nn(300),
     1  cpress(300),coords(2,2),time(2)

       call read(nn,cpress)

       do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
       end do

       if (temp(1) == 0.0) then
        temp(1) = 25.0
       end if
       if (temp(2) == 0.0) then
        temp(2) = 25.0
       end if
       
       p = d(2)
       tt = max(temp(1),temp(2))
       t = time(2)

       data sy_cu_temp/27,200,250,400,527/
       data sy_cu/69.4,58.51,54.45,36.94,26.27/
       data rho_temp/25,125,225,325,425,525,625,725,825,925/
       data rho_cu/1.71E-5,2.40E-5,3.09E-5,3.79E-5,4.52E-5,5.27E-5,
     1  6.05E-5,6.85E-5,7.68E-5,8.54E-5/
       data rho_cuzr/2.53E-5,3.32E-5,4.30E-5,5.49E-5,6.88E-5,8.46E-5,
     1  1.02E-4,1.22E-4,1.44E-4,1.68E-4/

       if (ciname == 'CU-CU') then
        p_avg = 32.25

        call fit(sy_cu_temp,sy_cu,tt,sy,size(sy_cu_temp))
        call fit(rho_temp,rho_cu,tt,rho_1,size(rho_temp))
        call fit(rho_temp,rho_cuzr,tt,rho_2,size(rho_temp))
        ecr = 4.34E-3*exp(-2.33E-2*tt) +
     1        3.0*0.05*sy/p_avg*(rho_1 + rho_2)*0.5

       else if (ciname == 'CU-AL') then
        ecr = 7.77E-3*exp(-3.09E-3*tt)
       end if

       tcr = ecr/(2.44E-8*(tt+273))
       ak(1) = 1.0/tcr

c      open(unit=23, file='c:\\temp\\contact_nodes.txt', access='append', status='old')
c      write(23,12) node
 12    format(i)


c       write(*,77) 'error_t',tt,d(2),node,tcr
 77    format (a, f6.2, f7.3, i, f7.3 /)

       return
      end subroutine gapcon

c     Computes and returns electrical contact resistance
      subroutine gapelectr(sigma,d,temp,predef,time,ciname,
     1 slname,msname,coords,node,npred,kstep,kinc)

       include 'aba_param_dp.inc'

       character*80 ciname,slname,msname
       real cpress,sy_cu_temp(5),sy_cu(5),rho_temp(10),rho_cu(10),
     1  rho_cuzr(10),sy,rho_1,rho_2,tt
       integer nn

       dimension sigma(5),d(2),temp(2),predef(2,*),time(2),
     1  coords(2,2),nn(300),cpress(300)

       call read(nn,cpress)

       do i = 1, size(nn)
        if (nn(i) .eq. node) then
         d(2) = cpress(i)
         exit
        else
         d(2) = 7.64
        end if
       end do

       if (temp(1) == 0.0) then
        temp(1) = 25.0
       end if
       if (temp(2) == 0.0) then
        temp(2) = 25.0
       end if

       p = d(2)
       tt = max(temp(1),temp(2))
       t = time(2)

       data sy_cu_temp/27,200,250,400,527/
       data sy_cu/69.4,58.51,54.45,36.94,26.27/
       data rho_temp/25,125,225,325,425,525,625,725,825,925/
       data rho_cu/1.71E-5,2.40E-5,3.09E-5,3.79E-5,4.52E-5,5.27E-5,
     1  6.05E-5,6.85E-5,7.68E-5,8.54E-5/
       data rho_cuzr/2.53E-5,3.32E-5,4.30E-5,5.49E-5,6.88E-5,8.46E-5,
     1  1.02E-4,1.22E-4,1.44E-4,1.68E-4/

       if (ciname == 'CU-CU') then
        p_avg = 32.25 
        call fit(sy_cu_temp,sy_cu,tt,sy,size(sy_cu_temp))
        call fit(rho_temp,rho_cu,tt,rho_1,size(rho_temp))
        call fit(rho_temp,rho_cuzr,tt,rho_2,size(rho_temp))
        ecr = 4.34E-3*exp(-2.33E-2*tt) +
     1        3.0*0.05*sy/p_avg*(rho_1 + rho_2)*0.5

       else if (ciname == 'CU-AL') then
        ecr = 7.77E-3*exp(-3.09E-3*tt)
       end if

       sigma(1) = 1.0/ecr

c       open(unit=23, file='c:\\temp\\cased\\contact_nodes.txt')
c       write(23,12) node
 12    format(i)

c       write(*,88) 'error_e',tt,d(2),node,ecr
 88    format (a, f6.2, f7.3, i, f20.6, /)

       return
      end subroutine gapelectr