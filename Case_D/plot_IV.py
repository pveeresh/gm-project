import matplotlib.pyplot as plt
import re

def getI(path):
    I = []

    with open(path) as fid:
        for c, line in enumerate(fid):
            z = float(line)
            if (z*1000)<=0.0: 
                I.append(1.0)
                continue
            I.append(z*1000)         
            if c*0.008>=30: break

    return I

def plot_EPOT():
    V_sim, V_sim_t, I, I_avg, = ([] for i in range(4))
    
    with open("EPOT_D.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            V_sim.append(float(z[0]))
            V_sim_t.append(float(z[-1]))

    paths = ["C:/temp/Expdata/D/I/_532.dta", "C:/temp/Expdata/D/I/_533.dta", "C:/temp/Expdata/D/I/_534.dta",
             "C:/temp/Expdata/D/I/_535.dta", "C:/temp/Expdata/D/I/_536.dta"]
    for j in range(0,len(paths)):
        I.append(getI(paths[j]))

    for k in range(len(I[0])):
        I_avg.append(sum(I[z][k] for z in range(3))/3)

    I_sim = I_avg[::63] # round(62.5)
    I_sim.insert(0,0.0)

    I_sim, V_sim = zip(*sorted(zip(I_sim, V_sim)))
    
    plt.rcParams["font.size"] = 18
    '''
    fig, ax1 = plt.subplots()
    ax1.plot(V_sim_t, V_sim, marker='o', color='#377EB8', label='Voltage')
    ax1.set_xlabel("Time (ms)")
    ax1.set_ylabel("Voltage (V)")
    ax1.tick_params(axis='y', labelcolor='#377EB8')
    ax1.spines['left'].set_color('#377EB8')
    ax1.yaxis.label.set_color('#377EB8')
    ax1.tick_params(axis='y', colors='#377EB8')
    
    ax2 = ax1.twinx()
    ax2.set_ylabel("Current (A)")
    ax2.plot(V_sim_t, I_sim, marker='d', color='#E41A1C', label='Current')
    ax2.tick_params(axis='y', labelcolor='#E41A1C')
    ax2.spines['right'].set_color('#E41A1C')
    ax2.yaxis.label.set_color('#E41A1C')
    ax2.tick_params(axis='y', colors='#E41A1C')
    '''
    
    plt.plot(I_sim, V_sim, marker='d', c='#377EB8')   
    plt.ylabel("Voltage (V)")
    plt.xlabel("Current (A)")
    

    plt.show()

if __name__=="__main__":
    plot_EPOT()