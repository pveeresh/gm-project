import matplotlib.pyplot as plt

'''Plots average interface temperature (averaged over contact nodes) over simuation time'''
def plot_NT():
    x, y, xx, yy = ([] for i in range(4))
    
    # Abaqus output
    with open("NT_D.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            y.append(float(z[0]))
            x.append(float(z[-1]))

    plt.rcParams["font.size"] = 18

    plt.plot(x, y, marker='d', color='#4DAF4A', label='Interface temperature')
    plt.ylabel("Average Interface Temperature (T)")
    plt.xlabel("Time (ms)")
    plt.legend(loc='upper right')
    plt.show()

if __name__=="__main__":
    plot_NT()