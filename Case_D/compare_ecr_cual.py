import matplotlib.pyplot as plt
import re
import csv
import numpy as np
import math
import matplotlib.ticker as mtick

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

def plot_ecr():
    r, time, temp, r_zhang, temp_zhang, r_wan = ([] for i in range(6))    

    Al_raw_resistivity = '''
    25  2.710E-05
    75  3.280E-05
    100 3.64E-05
    200 4.920E-05
    300 6.190E-05
    400 7.300E-05
    500 8.610E-05
    600 9.820E-05
    650 1.06E-04
    700 2.49E-04
    750 2.58E-04'''.split('\n')

    Cu110_raw_data = '''
    25    0.0000170978
    75    0.0000206022
    125   0.0000239916
    175   0.0000274119
    225   0.0000308710
    275   0.0000343747
    325   0.0000379275
    425   0.0000451944
    525   0.0000526959
    625   0.0000604525
    725   0.0000684832
    825   0.0000768068
    925   0.0000854429
    1025  0.0000854429'''.split('\n')

    Al_sy_raw_data = '''
    25  34.10
    50  33.58
    100 31.85
    150 28.85
    200 24.63
    250 18.99
    300 15.04
    350 11.89
    370 11.34'''.split('\n')    

    '''Time-dependent ECR obtained by averaging experimental results'''
    for t in frange(0,30.5,0.5):
        time.append(t)
        if t<2:
            r.append(-0.00064887*2.0 + 0.00962006)
        elif t>=2 and t<=8:
            r.append(-0.00064887*t + 0.00962006)
        elif t>8 and t<12:
            r.append(0.00025338*t + 0.0021783)       
        elif t>=12 and t<=18:
            r.append(-0.00030989*t + 0.00908174)
        elif t>18 and t<22:
            r.append(-2.94551899e-5*t + 3.96433301e-3)
        elif t>=22 and t<=28:
            r.append(-0.00025107*t + 0.00889666)
        elif t>28:
            r.append(-0.00025107*28.0 + 0.00889666) 

    '''Get average interface temperature from simulation'''
    with open("NT_D.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            temp.append(float(z[0]))

    y = [math.log(a) for a in r]
    K, A = np.polyfit(temp, y, 1)
    r_fit = [math.exp(A)*math.exp(K*a) for a in np.linspace(25.0, 750.0, 1000)]
    print(K)
    print(math.exp(A))


    '''Compute ECR using Zhang's and Wan's model'''
    Cu110 = np.array([[float(s) for s in r.split()] for r in Cu110_raw_data if r])
    Al_sy = np.array([[float(s) for s in r.split()] for r in Al_sy_raw_data if r])
    Al = np.array([[float(s) for s in r.split()] for r in Al_raw_resistivity if r])
    temp_zhang = np.linspace(25.0, 750.0, 1000)
    
    for i, T in enumerate(temp_zhang):
        sy = np.interp(T, Al_sy[:,0], Al_sy[:,1])
        Pavg = 26.62
        d = 0.05
       
        rho1 = np.interp(T, Cu110[:,0], Cu110[:,1]) 
        rho2 = np.interp(T, Al[:,0], Al[:,1]) 
        r_zhang.append(3.0*d*sy/Pavg*(rho1 + rho2)*0.5)
        r_wan_20 = (1.93e-8*Pavg**(-2/3) - 8.1e-9*Pavg**(-0.5) + 1.25e-9*Pavg**(-1/3))*1e6
        r_wan.append(r_wan_20*((sy/34.1)**1.3)*((rho1+rho2)/(2.710E-05+1.710E-05)))
        
        if T>=660: r_fit[i] = 1.0e-6 # Fit modified for when Al melts

    plt.rcParams["font.size"] = 18
    fig, ax = plt.subplots()

    plt.plot(temp_zhang, r_fit, c='#377EB8')
    #plt.plot(temp_zhang, r_fit, '#E41A1C', label='$ECR = %0.4f e^{%0.4f T}$' % (math.exp(A), K))
    #plt.plot(temp_zhang, r_wan, '#4DAF4A', label='Wans model')
    plt.ylabel(r'ECR$_{Cu/Al}$ ($\Omega$mm$^2$)')
    plt.xlabel("Temperature (C)")
    #ax.set_ylim([0,2e-5])
    #ax.set_xlim([2100,2150])
    #ax.margins(x=0)
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    #plt.legend()
    plt.show()

if __name__=="__main__":
    plot_ecr()