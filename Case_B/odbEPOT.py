from odbAccess import *
from sys import argv,exit

'''Gets maximum electric potential for every time step from Abaqus output file'''
def getMaxFO():
    odb = openOdb("B_TE_25micron.odb")

    FO = 'EPOT'
    fid = open("EPOT_B.txt","w")

    for step in odb.steps.values():
        print 'Processing Step:', step.name      
        for frame in step.frames:
            max_FO = 0 
            allFields = frame.fieldOutputs 
            nodeset = odb.rootAssembly.instances['PART-1-1'].nodeSets['TOP_ELEC_TOP_SURF']
            if (allFields.has_key(FO)):
                FOSet = allFields[FO]
                FOSet = FOSet.getSubset(region=nodeset)  
                for FOValue in FOSet.values:
                    if FOValue.data>max_FO: max_FO=FOValue.data

            fid.write(str(round(max_FO,5)) + " " + frame.description)
            fid.write("\n")
    odb.close()

if __name__ == '__main__':
    getMaxFO()