import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import numpy as np

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

def getI(path):
    I = []

    with open(path) as fid:
        for c, line in enumerate(fid):
            z = float(line)
            if (z*1000)<=0.0: 
                I.append(1.0)
                continue
            I.append(z*1000)         
            if c*0.008>=30: break

    return I
    
'''Computes average current'''
def modify_current_input():
    I, I_avg, t = ([] for i in range(3))

    paths = ["C:/temp/Expdata/B/I/_526.dta", "C:/temp/Expdata/B/I/_527.dta", "C:/temp/Expdata/B/I/_528.dta"]    

    plt.rcParams["font.size"] = 18 
    color = iter(['b','r','g'])

    for i in frange(0, 30.0, 0.008):
        t.append(i)

    for j in range(0,len(paths)):
        I.append(getI(paths[j]))
        c = next(color)
        plt.plot(t, I[j], c=c, linewidth=2.0)

    for k in range(len(I[0])):
        I_avg.append(sum(I[z][k] for z in range(3))/3)

    plt.plot(t, I_avg, 'k--', linewidth=2.0, label='Average current')

    plt.xlabel('Time (ms)')
    plt.ylabel('Current (A)')
    plt.legend()
    plt.show()

    I_surf = [z/87.3 for z in I_avg]
    max_I = max(I_surf)
    I_amp = [z/max_I for z in I_surf]

    '''Writes current input as a fraction of the maximum current ('magnitude' in Abaqus)'''
    with open("current_load_caseB.dat", "w+") as fidw:
        for y,z in zip(t,I_amp):
            z = format(z, '0.5f')
            y = format(y, '0.3f')
            line = y + ", " + z
            fidw.write(line)
            fidw.write("\n")
    print(max_I)

if __name__=="__main__":
    modify_current_input()     