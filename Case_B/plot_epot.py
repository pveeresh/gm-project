import matplotlib.pyplot as plt
import re
import matplotlib as mpl

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

def getIV(path):
    V = []           
    
    c = 0          
    with open(path) as fid:
        for line in fid:
            z = float(line)
            if z<0: 
                V.append(0)
                c += 1
                continue
            V.append(z)
            c += 1
            if c*0.008>30: break
    
    return V

'''Computes average experimental electric potential and its error (90% confidence interval and using t-distribution)
   and compares this with the simulation electric potential'''
def plot_EPOT():
    x, y, t, V, u, s, error = ([] for i in range(7))

    paths = ["C:/temp/Expdata/B/V/_526.dta", "C:/temp/Expdata/B/V/_527.dta", "C:/temp/Expdata/B/V/_528.dta"]    
    
    with open("EPOT_B.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            y.append(float(z[0]))
            x.append(float(z[-1]))

    for i in frange(0, 30.0, 0.008):
        t.append(i)

    for j in range(0,len(paths)):
        V.append(getIV(paths[j]))

    l = 3
    for i in range(len(V[0])):
        u.append(sum(V[z][i] for z in range(l))/l)
        s.append((sum((V[a][i]-u[i])**2 for a in range(l))/(l-1))**0.5)
        error.append((2.92*s[i])/(l**0.5))

    plt.rcParams["font.size"] = 18

    plt.fill_between(t, [x-y for x,y in zip(u,error)], [x+y for x,y in zip(u,error)], color='#E41A1C', alpha=0.25)
    plt.plot(x, y, marker='d', color='#4DAF4A', label='Simulation')   
    plt.plot(t, u, color='#E41A1C', label='Experimental_average')
    #plt.plot(t, [x+y for x,y in zip(u,error)], '--', c='#377EB8', label='Experimental_threshold')
    #plt.plot(t, [x-y for x,y in zip(u,error)], '--', c='#377EB8')
    plt.ylabel("Voltage (V)")
    plt.xlabel("Time (ms)")
    plt.legend(loc='upper left')
    plt.show()

if __name__=="__main__":
    plot_EPOT()