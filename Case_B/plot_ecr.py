import matplotlib.pyplot as plt
import re
import csv
import numpy as np
import math
import matplotlib.ticker as mtick

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

'''Fits exponential curve to ECR vs temp'''
def plot_ecr():
    r, time, temp, r_wan, temp_wan = ([] for i in range(5))
    
    for t in frange(0,30.5,0.5):
        time.append(t)
        if t<2:
            r.append(-0.00016009*2.0 + 0.00292445)
        elif t>=2 and t<=8:
            r.append(-0.00016009*t + 0.00292445)
        elif t>8 and t<12:
            r.append(5.69396409e-5*t + 1.18997029e-3)       
        elif t>=12 and t<=18:
            r.append(-0.00015815*t + 0.003771)
        elif t>18 and t<22:
            r.append(8.46363606E-5*t + -5.97143388E-4)
        elif t>=22 and t<=28:
            r.append(-9.65760833e-5*t + 3.38953038e-3)
        elif t>28:
            r.append(-9.65760833e-5*28 + 3.38953038e-3)          

    with open("NT_B_25micron.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            temp.append(float(z[0]))

    y = [math.log(a) for a in r]
    K, A = np.polyfit(temp, y, 1)
    fit_y = [math.exp(A)*math.exp(K*a) for a in temp]
    print('{:.2e}'.format(K))
    print('{:.2e}'.format(math.exp(A)))

    '''
    r_print = ["%.6f" % a for a in r]
    with open("ECR_CuCuZr.txt", 'w') as fidw:
        writer = csv.writer(fidw, delimiter='\t')
        writer.writerows(zip(r_print,time))
    '''

    plt.rcParams["font.size"] = 18
    fig, ax = plt.subplots()

    #plt.plot(time, r, color='b', label='Cu/CuZr')   
    plt.scatter(temp, r, color='#377EB8', s=20)
    plt.plot(temp, fit_y, color='#E41A1C', label='$ECR = %0.4f e^{%0.3f T}$' % (math.exp(A), K))
    plt.ylabel(r'ECR$_{Cu/CuZr}$ ($\Omega$mm$^2$)')
    plt.xlabel("Average Interface Temperature (C)")
    #plt.legend(loc='upper right')
    ax.set_ylim([0,0.0035])
    ax.set_xlim([24,80])
    plt.locator_params(axis='y', nbins=10)
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax.tick_params(direction="in")
    ax.margins(x=0)
    plt.legend()
    plt.show()

if __name__=="__main__":
    plot_ecr()