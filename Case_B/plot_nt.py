import matplotlib.pyplot as plt
import re

'''Plots average interface temperature (averaged over contact nodes) over simuation time'''
def plot_EPOT():
    x, y, xx, yy, xxx, yyy, xxxx, yyyy = ([] for i in range(8))
    
    with open("NT_B_25micron.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            y.append(float(z[0]))
            x.append(float(z[-1]))

    with open("NT_B_50micron.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            yy.append(float(z[0]))
            xx.append(float(z[-1]))

    with open("NT_B_12_5micron.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            yyy.append(float(z[0]))
            xxx.append(float(z[-1]))

    with open("NT_B_100micron.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            yyyy.append(float(z[0]))
            xxxx.append(float(z[-1]))

    plt.rcParams["font.size"] = 18

    plt.plot(x, y, marker='d', color='#4DAF4A', label='Interface temperature')
    #plt.plot(xxx, yyy, color='b', label=r'h$_{Cu}$ = 12.5$\mu$m')
    #plt.plot(x, y, color='#4DAF4A', label=r'h$_{Cu}$ = 25$\mu$m')   
    #plt.plot(xx, yy, color='k', label=r'h$_{Cu}$ = 50$\mu$m')
    #plt.plot(xxxx, yyyy, color='r', label=r'h$_{Cu}$ = 100$\mu$m')
    plt.ylabel("Average Interface Temperature (T)")
    plt.xlabel("Time (ms)")
    #plt.legend(loc='upper left')
    plt.show()

if __name__=="__main__":
    plot_EPOT()