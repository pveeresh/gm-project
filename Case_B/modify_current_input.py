import matplotlib.pyplot as plt

'''Samples input current '''
def modify_current_input():
    I_exp, t_exp, I_sim, t_sim = ([] for i in range(4))
    max = 0
    with open("_527_I.dta") as fid:
        for c, line in enumerate(fid):
            z = float(line)
            I_exp.append(z)
            t_exp.append(c*0.008)

            if (c*0.008)%1.0==0:
                I_sim.append(z)
                t_sim.append(c*0.008)
            
            z = (z/87.3)*1000

            if z>max: max=z
            if (c*0.008>=30): break

    plt.rcParams["font.size"] = 18
    plt.step(t_sim, I_sim, where='pre', color='b', label='Simulation')
    plt.plot(t_exp, I_exp, color='r', label='Experimental')
    
    plt.xlabel('Time (ms)')
    plt.ylabel('Current (kA)')
    plt.legend()
    plt.show()
    #exit()
    
    '''Writes current input as a fraction of the maximum current ('magnitude' in Abaqus)'''
    with open("_527_I.dta") as fid, open("current_load_caseB.dat", "w+") as fidw:
        for c, line in enumerate(fid):
            line = line.rstrip()
            line = line.lstrip()
            x = float(line)
            x = ((x/87.3)*1000)/max       
            x = format(x, '0.5f')
            line = str(round(c*0.008,3)) + ", " + x
            fidw.write(line)
            fidw.write("\n")
            if (c*0.008>=30): break
    print(max)

if __name__=="__main__":
    modify_current_input()     