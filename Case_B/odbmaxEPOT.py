from odbAccess import *
from sys import argv,exit

'''Computes average electric potential (averaged over entire surface) for every contact surface in one time step'''
def getMaxFO():
    surfaces = ['TOP_ELEC_TOP_SURF', 'TOP_ELEC_BOT_SURF', 'CU_BUS_TOP_SURF', 'CU_BUS_BOT_SURF', 'BOT_ELEC_TOP_SURF', 'BOT_ELEC_BOT_SURF']
    
    odb = openOdb("B_TE.odb")

    FO = 'EPOT'
    ts = 26
    fid = open("EPOT_%s.txt"%ts,"w")

    for step in odb.steps.values():
        print 'Processing Step:', step.name       
        frame = step.frames[ts]
        allFields = frame.fieldOutputs
        for surf in surfaces:
            nodeset = odb.rootAssembly.instances['PART-1-1'].nodeSets[surf]
            sum_FO = 0
            c = 0
            if (allFields.has_key(FO)):
                FOSet = allFields[FO]
                FOSet = FOSet.getSubset(region=nodeset)  
                for FOValue in FOSet.values:
                    sum_FO += FOValue.data
                    c += 1
            mean_FO = sum_FO/c
            fid.write(str(round(mean_FO,5)) + " " + surf)
            fid.write("\n")
    odb.close()

if __name__ == '__main__':
    getMaxFO()