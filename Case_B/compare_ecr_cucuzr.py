import matplotlib.pyplot as plt
import re
import csv
import numpy as np
import math
import matplotlib.ticker as mtick

def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step

def plot_ecr():
    r, time, temp, r_zhang, temp_zhang = ([] for i in range(5))    

    CuZr_raw_resistivity = '''
    25  2.53E-05
    100 3.10E-05
    200 4.04E-05
    300 5.17E-05
    400 6.51E-05
    500 8.05E-05
    600 9.78E-05
    700 1.17E-04
    800 1.39E-04
    900 1.62E-04'''.split('\n')

    Cu110_raw_data = '''
    25    0.0000170978
    75    0.0000206022
    125   0.0000239916
    175   0.0000274119
    225   0.0000308710
    275   0.0000343747
    325   0.0000379275
    425   0.0000451944
    525   0.0000526959
    625   0.0000604525
    725   0.0000684832
    825   0.0000768068
    925   0.0000854429
    1025  0.0000854429'''.split('\n')

    Cu110_sy_raw_data = '''
    27  69.40
    200 58.51
    250 54.45
    400 36.94
    527 26.27'''.split('\n')    

    '''Time-dependent ECR obtained by averaging experimental results'''
    for t in frange(0,30.5,0.5):
        time.append(t)
        if t<2:
            r.append(-0.00016009*2.0 + 0.00292445)
        elif t>=2 and t<=8:
            r.append(-0.00016009*t + 0.00292445)
        elif t>8 and t<12:
            r.append(5.69396409e-5*t + 1.18997029e-3)       
        elif t>=12 and t<=18:
            r.append(-0.00015815*t + 0.003771)
        elif t>18 and t<22:
            r.append(8.46363606E-5*t + -5.97143388E-4)
        elif t>=22 and t<=28:
            r.append(-9.65760833e-5*t + 3.38953038e-3)
        elif t>28:
            r.append(-9.65760833e-5*28 + 3.38953038e-3) 

    '''Get average interface temperature from simulation'''
    with open("NT_B_25micron.txt") as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            temp.append(float(z[0]))

    y = [math.log(a) for a in r]
    K, A = np.polyfit(temp, y, 1)
    r_fit = [math.exp(A)*math.exp(K*a) for a in np.linspace(25.0, 1000.0, 1000)]

    '''Compute ECR using Zhang's model'''
    Cu110 = np.array([[float(s) for s in r.split()] for r in Cu110_raw_data if r])
    Cu110_sy = np.array([[float(s) for s in r.split()] for r in Cu110_sy_raw_data if r])
    CuZr = np.array([[float(s) for s in r.split()] for r in CuZr_raw_resistivity if r])
    temp_zhang = np.linspace(25.0, 1000.0, 1000)
    
    for i,T in enumerate(temp_zhang):
        sy = np.interp(T, Cu110_sy[:,0], Cu110_sy[:,1])
        Pavg = 32.25
        d = 0.05
       
        rho1 = np.interp(T, Cu110[:,0], Cu110[:,1]) 
        rho2 = np.interp(T, CuZr[:,0], CuZr[:,1]) 
        r_fit[i] += 3.0*d*sy/Pavg*(rho1 + rho2)*0.5

    plt.rcParams["font.size"] = 18
    fig, ax = plt.subplots()

    #plt.plot(temp_zhang, r_fit, '#E41A1C', label='$ECR = %0.4f e^{%0.3f T}$' % (math.exp(A), K))
    #plt.plot(temp_zhang, r_zhang, '#4DAF4A', label='Zhangs model')
    plt.plot(temp_zhang, r_fit, c='#377EB8')
    plt.ylabel(r'ECR$_{Cu/CuZr}$ ($\Omega$mm$^2$)')
    plt.xlabel("Temperature (C)")
    #plt.legend(loc='upper right')
    #ax.set_ylim([0,2.5e-5])
    #ax.set_xlim([250,300])
    ax.margins(x=0)
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    ax.tick_params(direction="in")
    #plt.legend()
    plt.show()

if __name__=="__main__":
    plot_ecr()