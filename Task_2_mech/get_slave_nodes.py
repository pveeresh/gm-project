import sys
import re

def get_slave_nodes(path):
    slave_bus_elec, slave_elec_tab, slave_tab_bus, slave_tab_tab = ([] for i in range(4))
    with open(path) as fidr:
        for line in fidr:            
            line = line.lstrip()
            if line.startswith("CONTACT OUTPUT"):
                if "BUS-ELEC" in line:
                    slave_bus_elec, line, fidr = read_slave_surf(line, fidr)

                if "ELEC-TAB" in line:
                    slave_elec_tab, line, fidr = read_slave_surf(line, fidr)   

                if "TAB-BUS" in line:
                    slave_tab_bus, line, fidr = read_slave_surf(line, fidr)   

                if "TAB-TAB" in line:
                    slave_tab_tab, line, fidr = read_slave_surf(line, fidr)

    return slave_bus_elec, slave_elec_tab, slave_tab_bus, slave_tab_tab



def read_slave_surf(line, fidr):
    slave = []        
    for line in fidr:
        line = line.rstrip()
        line = line.lstrip() 
        if re.findall('^\s*[0-9]', line):
            x = list(map(str,line.split()))
            x = x[::len(x)-1]
            x = list(map(float,x))
            slave.append(x)           

        elif line.startswith("CONTACT OUTPUT"): break
        elif line=='N O D E   O U T P U T': break
    
    return [i[0] for i in slave], line, fidr