import sys
import re
import os

def read_disp(path_read):
    node_r = []
    with open(path_read) as fidr:
        for line_r in fidr:  
            line_r = line_r.lstrip()
            if line_r.startswith("N O D E   O U T P U T"):
                for line_r in fidr:
                    line_r = line_r.rstrip()
                    line_r = line_r.lstrip()
                    if re.findall('^\s*[0-9]', line_r):
                        x = list(map(str,line_r.split())) 
                        x = list(map(float,x))
                        x[0] = int(float(x[0]))
                        node_r.append(x)

                    elif line_r=="THE ANALYSIS HAS BEEN COMPLETED": break

    return node_r

def calculate_disp(path_prev,path_curr):
    node_p = read_disp(path_prev)
    node_c = read_disp(path_curr)
    node_u = []

    for i in range(len(node_p)):
        x = node_c[i][1] - node_p[i][1]
        y = node_c[i][2] - node_p[i][2]
        node_u.append([node_c[i][0], x, y])
    
    return node_u