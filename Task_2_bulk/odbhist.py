#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
from odbAccess import *
from sys import argv,exit

def getMaxFO():
   fid = open("ALLJD_sample3_task2.txt","w")
   sum = 0
   for i in range(1,135):
        odbname = "Electrical_thermal_%s.odb"%i
        odb = openOdb(odbname)
        HO = 'ALLJD'
        step = odb.steps['Electrical_thermal']
        HOSet = step.historyRegions['Assembly Assembly-1'].historyOutputs[HO]
        
        sum += HOSet.data[-1][1]

        fid.write(str(round(sum,5)) + " " + str(i))
        fid.write("\n")
        odb.close()

if __name__ == '__main__':
    getMaxFO()