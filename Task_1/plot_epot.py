import matplotlib.pyplot as plt
import re
import matplotlib as mpl

def plot_EPOT():
    file = "EPOT_sample3.txt"
    x, y, xx, yy = ([] for i in range(4))
    
    with open(file) as fid:
        for line in fid:           
            z = list(map(str,line.split()))
            y.append(float(z[0]))
            x.append(float(z[-1]))
    
    c = 1
    with open("Sample#3_V.dta") as fid:
        for line in fid:
            if c > 17000: break
            yy.append(float(line)*(-1))
            xx.append(0.008*c)
            c += 1

    mpl.rcParams["font.size"] = 18

    '''fig, ax1 = plt.subplots()
    ax1.plot(x, y, marker='o', color='b', label='Voltage')
    ax1.set_xlabel("Time (ms)", fontsize=18)
    ax1.set_ylabel("Voltage (V)", fontsize=18)
    
    ax2 = ax1.twinx()
    ax2.set_ylabel("Current (kA)", fontsize=18)
    ax2.plot(xx, yy, marker='o', color='r', label='Current')
    ax2.tick_params(axis='y', labelcolor='r')

    fig.tight_layout()'''

    plt.plot(x, y, marker='o', color='b', label='Simulation')
    plt.plot(xx, yy, color='r', label='Experimental')
    plt.ylabel("Voltage (V)")
    plt.xlabel("Time (ms)")
    plt.legend()
    plt.show()

if __name__=="__main__":
    plot_EPOT()