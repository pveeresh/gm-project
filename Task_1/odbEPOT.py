from odbAccess import *
from sys import argv,exit

def rightTrim(input,suffix):
    if (input.find(suffix) == -1):
        input = input + suffix
    return input

def getMaxFO(odbName):
    
    odb = openOdb(odbName)

    FO = 'EPOT'
    fid = open("EPOT_sample3.txt","w")

    for step in odb.steps.values():
        print 'Processing Step:', step.name      
        for frame in step.frames:
            max_FO = 0 
            allFields = frame.fieldOutputs 
            nodeset = odb.rootAssembly.instances['PART-1-1'].nodeSets['TOP_ELEC_TOP_SURF']
            if (allFields.has_key(FO)):
                FOSet = allFields[FO]
                FOSet = FOSet.getSubset(region=nodeset)  
                for FOValue in FOSet.values:
                    if FOValue.data>max_FO: max_FO=FOValue.data

            fid.write(str(round(max_FO,5)) + " " + frame.description)
            fid.write("\n")
    odb.close()

if __name__ == '__main__':
    
    odbName = None
    argList = argv
    argc = len(argList)
    i=0
    while (i < argc):
        if (argList[i][:2] == "-o"):
            i += 1
            name = argList[i]
            odbName = rightTrim(name,".odb")
        elif (argList[i][:2] == "-h"):            
            print __doc__
            exit(0)
        i += 1
    if not (odbName):
        print ' **ERROR** output database name is not provided'
        print __doc__
        exit(1)
    getMaxFO(odbName)