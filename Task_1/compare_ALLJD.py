#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
import matplotlib.pyplot as plt
import re
import matplotlib as mpl

'''
Energy is expected to quadruple when current density is doubled.
'''

def plot_ALLJD():
    c = 1
    E_1, t_1, E_2, t_2 = ([] for i in range(4))
    with open("compare_ALLJD.rpt") as fid:
        for line in fid:
            if "_temp_1" in line:
                for line in fid:
                    if "_temp_2" in line: break
                    if re.findall('^\s*[0-9]', line):
                        x = list(map(str,line.split()))
                        x = list(map(float,x))
                        t_1.append(x[0])
                        E_1.append(x[1]/1000)
                    
            if "_temp_2" in line:
                for line in fid:
                    if re.findall('^\s*[0-9]', line):
                        x = list(map(str,line.split()))
                        x = list(map(float,x))
                        t_2.append(x[0])
                        E_2.append(x[1]/1000)

    mpl.rcParams["font.size"] = 18
    plt.plot(t_1,E_1, marker='o', color='b', label='10 kA')
    plt.annotate(str(round(E_1[-1],1)),xy=(t_1[-1],E_1[-1]))
    plt.xlim([0,355])
    plt.ylim([0,1400])
    plt.plot(t_2,E_2, marker='o', color='r', label='5 kA')
    plt.annotate(str(round(E_2[-1],2)),xy=(t_2[-1],E_2[-1]))
    plt.xlabel("Time (ms)", fontsize=18)
    plt.ylabel("Energy (J)", fontsize=18)
    plt.legend()
    plt.show()
            


if __name__=="__main__":
    plot_ALLJD()
