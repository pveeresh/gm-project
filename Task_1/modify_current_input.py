#!\C:\Users\pawan\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Python 3.7
import matplotlib.pyplot as plt
import matplotlib as mpl

def modify_current_input():
    I_exp, t_exp, I_sim, t_sim = ([] for i in range(4))
    max = 0
    c = 1
    with open("Sample#3_I.dta") as fid:
        for line in fid:
            z = float(line)
            I_exp.append(z)
            t_exp.append(c*0.008)

            if (c*0.008)%1.0==0:
                I_sim.append(z)
                t_sim.append(c*0.008)
            
            z = (z/87.3)*1000

            if z>max: max=z
            c += 1
            if (c*0.008>135): break

    mpl.rcParams["font.size"] = 18
    plt.step(t_sim, I_sim, where='pre', color='b', label='Simulation')
    plt.plot(t_exp, I_exp, color='r', label='Experimental')
    
    plt.xlabel('Time (ms)')
    plt.ylabel('Current (kA)')
    plt.legend()
    plt.show()
    #exit()
    
    '''Writes current input as a fraction of the maximum current ('magnitude' in Abaqus)'''
    c = 1
    with open("Sample#3_I.dta") as fid, open("current_load_sample3.dat", "w+") as fidw:
        for line in fid:
            line = line.rstrip()
            line = line.lstrip()
            x = float(line)
            x = ((x/87.3)*1000)/max       
            x = format(x, '0.5f')
            line = str(round(c*0.008,3)) + ", " + x
            fidw.write(line)
            fidw.write("\n")
            c += 1
            if (c*0.008>135): break
    print(max)

if __name__=="__main__":
    modify_current_input()     