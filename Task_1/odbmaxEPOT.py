from odbAccess import *
from sys import argv,exit

def rightTrim(input,suffix):
    if (input.find(suffix) == -1):
        input = input + suffix
    return input

def getMaxFO(odbName):
    surfaces = ['TOP_ELEC_TOP_SURF', 'TOP_ELEC_BOT_SURF', 'CU_TAB_TOP_SURF', 'CU_TAB_BOT_SURF', 'AL_TAB_TOP_SURF', 
                'AL_TAB_BOT_SURF', 'CU_BUS_TOP_SURF', 'CU_BUS_BOT_SURF', 'BOT_ELEC_TOP_SURF', 'BOT_ELEC_BOT_SURF']
    
    odb = openOdb(odbName)

    FO = 'EPOT'
    ts = 20
    fid = open("EPOT_%s.txt"%ts,"w")

    for step in odb.steps.values():
        print 'Processing Step:', step.name       
        frame = step.frames[ts]
        allFields = frame.fieldOutputs
        for surf in surfaces:
            nodeset = odb.rootAssembly.instances['PART-1-1'].nodeSets[surf]
            sum_FO = 0
            c = 0
            if (allFields.has_key(FO)):
                FOSet = allFields[FO]
                FOSet = FOSet.getSubset(region=nodeset)  
                for FOValue in FOSet.values:
                    sum_FO += FOValue.data
                    c += 1
            mean_FO = sum_FO/c
            fid.write(str(round(mean_FO,5)) + " " + surf)
            fid.write("\n")
    odb.close()

if __name__ == '__main__':
    
    odbName = None
    argList = argv
    argc = len(argList)
    i=0
    while (i < argc):
        if (argList[i][:2] == "-o"):
            i += 1
            name = argList[i]
            odbName = rightTrim(name,".odb")
        elif (argList[i][:2] == "-h"):            
            print __doc__
            exit(0)
        i += 1
    if not (odbName):
        print ' **ERROR** output database name is not provided'
        print __doc__
        exit(1)
    getMaxFO(odbName)